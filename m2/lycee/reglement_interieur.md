# Règlement intérieur



## Dura lex ...



Le règlement intérieur, c'est le texte de loi du lycée.



Il précise les droits et les devoirs de chacun dans la vie du lycée. Bien entendu, ce règlement implique aussi des devoirs de la part des enseignants.



On ne peut sanctionner un élève pour une faute non prévue dans le règlement intérieur ni utiliser pour un élève une sanction non prévue.



Il faut donc le connaître, au moins dans ses grandes lignes pour éviter, lors d'une gestion de conflit, de reprocher un comportement à tort ou prononcer une sanction irréalisable... ce qui ne ferait que compliquer le conflit et envenimer la situation.



Il faut être très clair lors d'un conflit avec un élève qu'on ne parvient pas à remettre dans le droit chemin avec une remarque orale :

* ton comportement est le suivant ... 
* le règlement intérieur pour ce comportement prévoit ceci ....
* dura lex ced lex.



## Sanctions



Le règlement prévoit un _arsenal_ de sanctions. 



Ne pas utiliser tout de suite la marche la plus haute ! Que feriez vous ensuite ??



La sanction doit être adaptée à la _gravité_ de la faute et à la récidive... Sinon elle conduira à une situation pire encore et risque d'être contestée.



La plupart du temps, lorsque c'est possible, il vaut mieux éviter de sanctionner mais plutôt prévenir, demander un changement d'attitude, bref discuter avant de sortir les armes. La grande majorité des conflits, pris à temps, se résolvent vite et calmement.



Il ne faut pas hésiter, avant de prendre une sanction ou pour expliquer pourquoi on l'a prise, à contacter les parents soit via l'ENT soit via le carnet de correspondance. Ce dernier garde une trace facilement vérifiable de vos remarques et les parents, prévenus, doivent prendre leur part de travail éducatif à la maison. 



## Un exemple de règlement



[Voici un exemple de règlement intérieur](reglement_interieur.pdf).



### Quelques questions :



* Un groupe d'élèves souhaitent créer un journal du lycée. Ils veulent pouvoir faire des articles sur divers sujet. Est-ce possible ? Sous quelle(s) condition(s) ?
* Un élève vous dit qu'il a le droit de ne pas travailler tant qu'il ne gêne personne et n'empêche pas son voisinage de travailler. Qu'en pensez-vous ? 
* Un élève est absent à une évaluation importante. La raison de son absence est parfaitement justifiée, connue des parents et un certificat médical est fourni. Peut-il refuser de repasser l'évaluation ?
* Quelles actions sont prévues pour lutter contre l'absentéisme des élèves?
* Peut-on décider d'exclure un élève de sa classe ? SI oui, sous quelles conditions ? Sinon pourquoi ?
* Les parents peuvent-ils ne pas accepter le règlement intérieur ? 