# Les instances du Lycée



## Questionnaire



Vous trouverez ci-après une description des plus importantes instances du lycée.

Consultez la pour répondre aux questions ce-dessous.



### Composition



1. Quelles instances sont présidées par le chef d'établissement ?
2. Quelles instances comptent impérativement des enseignants dans leur composition ?
3. Quelles instances comptent impérativement des parents d'élève(s) dans leur composition ?
4. Quelles instances comptent impérativement des élèves dans leur composition ?
5. Quelle instance peut voir intervenir un avocat ?
6. Quelle instance est composée en partie de membres élus ?
7. Quelles instances comportent impérativement un CPE au moins ?
8. Quelle(s) instance(s) fait(font) partie des obligation de service d'un enseignant ?
9. Qui décide de la composition du conseil pédagogique ?
10. Dans quelle instance peuvent siéger les membres d'un conseil municipal, des pompiers ou des policiers ?



### Rôle général

1. Quelle(s) instance(s) a(ont) un pouvoir décisionnaire ?
2. Rangez chaque instance sous un ou plusieurs label(s) parmi les labels suivants :
    * rôle administratif, lié fonctionnement de l'établissement.
    * rôle pédagogique.
    * rôle lié à l'organisation de la vie dans le lycée (bien être, sécurité, environnement ...)
    * rôle lié au domaine de la discipline des élèves.

3. Y-a-t-il une instance qui _gouverne_ toutes les autres ? Si oui, laquelle ?
4. Décrivez le rôle d'un conseil de classe.
5. Un conseil de classe peut-il sanctionner un élève pour manque de travail ou mauvais comportement ?
6. Sous quelle condition concernant un faute commise par un élève, cet élève peut-il être sanctionné pour cette faute ?
7. Quelle est la différence notable entre commission éducative et conseil de discipline ?
8. Quelles commissions interviennent dans la création du projet d'établissement ? 
9. Quelle commission a un rapport direct avec le COVID19 ?
10. Si des lycéens souhaitent voir aménager une zone disponible du lycée pour travailler ou se reposer, à quelle commission doivent-ils demander de faire remonter leurs idées ?
11. Quels élèves représentent les lycéens au CVL ?



### Questions diverses.

1. Citez une décision prise au CA qui impacte directement la répartition classe entière/demi-groupe de votre discipline ? Dans quelle autre instance, discute-t-on de ce sujet ?
2. Quelle décision importante du CA nécessite obligatoirement la tenue d'une commission permanente ?
3. Vous avez besoin de matériel informatique. Où en discuter ? 
4. Est-il possible d'assister à un conseil pédagogique si on n'en est pas membre ? En quoi cela peut-il être intéressant pour vous ?
5. La question du harcèlement scolaire est centrale :
    * Quelle commission décide des actions pour lutter contre ce problème ?
    * Où doivent être stipulés les sanctions en cas de harcèlement avéré ?

6. Vous voulez organiser une sortie scolaire. Où en demander l'autorisation ? Quelle commission vous l'accordera (ou pas ...) ?



____________



## Le CA



Il s'agit du **C**onseil **A**dministratif, présidé par le chef d'établissement. 

**C'est l'assemblée qui prend les décisions  importantes de l'organisation de l'établissement**. Il est composé  notamment de membres de la direction (proviseur, adjoint, gestionnaire), d'un CPE et de représentants élus (des  personnels de l'établissement, enseignants, élèves et de parents d'élèves). Il se  réunit au moins 3 fois par an pour adopter des décisions ou donner son  avis sur des sujets particuliers.



Le CA adopte notamment, sur le rapport du chef d'établissement :

- les décisions qui relèvent de l'autonomie de l'établissement en matière  pédagogique et éducative notamment les règles d'organisation de  l'établissement comme la répartition des moyens horaires entre les disciplines (**D**otation **G**lobal **H**oraire)
- le [projet d'établissement,](projet_etablissement.md)
- le [règlement intérieur](reglement_interieur.md) de l'établissement,
- le rapport relatif au fonctionnement pédagogique de l'établissement et à ses conditions matérielles de fonctionnement,
- le budget et le compte financier,
- les questions relatives à l'accueil, à l'information et à la participation des parents d'élèves à la vie scolaire,
- les questions relatives à l'hygiène, à la santé, à la sécurité,
- le plan de prévention de la violence incluant un programme d'action contre toutes les formes de harcèlement.



Le CA donne son accord sur :

- les orientations relatives à la conduite du dialogue avec les parents d'élèves
- le programme de l'association sportive,
- la programmation et les modes de financement des voyages scolaires.
- l'adhésion à tout groupement d'établissements,
- la conclusion des marchés,  conventions et  contrats dont l'établissement est signataire (à l'exception de certains marchés),
- le programme d'actions établi chaque année par le conseil école-collège,
- les modes de participation au plan d'action du groupement d'établissements pour la formation des adultes.



Le CA, sur recours du chef d'établissement, donne son avis sur :

- les propositions de créations et suppressions de sections, options et  formations complémentaires d'initiative locale dans l'établissement,
- les choix des manuels scolaires, des logiciels et des outils pédagogiques.
- la modification proposée par le maire des heures d'entrée et de sortie de l'établissement.





## La Commission permanente



La commission permanente, émanation du conseil d’administration (ses membres sont issus de ceux du CA et représentent chaque groupe présent au CA. Elle est également présidée par le chef d'établissement). Elle permet aux membres de la communauté éducative de réfléchir et réagir aux propositions avant la tenue du CA.de discuter au préalable les questions délicates qui seront soumises au CA. 



Elle est obligatoirement saisie des questions touchant à l’autonomie pédagogique et éducative de l’établissement.





## Le conseil pédagogique

Le conseil pédagogique est présidé par le chef d'établissement qui en désigne les membres. Il peut également inviter un intervenant non membre du conseil s'il le juge pertinent.

Il s'agit d'une instance de consultation (c'est toujours le CA qui tranche) des enseignants sur la politique éducative de l’établissement qui favorise la concertation entre les professeurs et participe à l’autonomie pédagogique des établissements.



La loi fixe la composition minimale du conseil pédagogique : 

* au moins un professeur principal de chaque niveau d’enseignement 
*  au moins un professeur par champ disciplinaire
* un conseiller principal d’éducation 
* le cas échéant le chef de travaux.



Il a pour mission de favoriser la concertation entre les professeurs, notamment pour :

* coordonner les enseignements, la  notation et l’évaluation des activités scolaires.
* les propositions d’expérimentations pédagogiques.
* Discuter autour de la répartition de la dotation des moyens par discipline.
*  préparer la partie  pédagogique du projet d’établissement. Dans ce cadre, le choix des  sujets traités et du fonctionnement interne est laissé à l’appréciation  du conseil pédagogique, dans le respect de la liberté pédagogique des  enseignants et du champ de compétence des personnels de direction. 



## Le conseil d'enseignement



Un conseil d'enseignement réunit les professeurs par discipline ou spécialité, sous la présidence du chef d'établissement. Il peut également permettre le travail en équipe inter-disciplinaire.

Les conseils d’enseignement ont pour fonction de favoriser les coordinations nécessaires entre les enseignants, en particulier  pour le choix des matériels techniques, des manuels et des supports  pédagogiques.

La participation aux conseils d’enseignement fait partie des obligations de service des enseignants.





## Le conseil de classe



**Il est chargé du suivi des élèves et des questions pédagogiques intéressant la vie de la classe**. Composé de membres du  personnel de l'établissement, de délégués d'élèves et de parents  d'élèves,  il se réunit au moins 3 fois par an. Il formule des  propositions concernant l'orientation et l'accompagnement des élèves et en informe les parents.



Le conseil de classe est composé des membres suivants :

- Chef d'établissement qui le préside et peut délégué au proviseur adjoint
- Professeurs de la classe
- 2 délégués des élèves
- 2 délégués de parents d'élèves
- Conseiller principal d'éducation 
- Conseiller d'orientation-psychologue quand cela lui est possible.
- Médecin scolaire, assistant social ou infirmier, quand c'est utile pour le cas personnel d'un élève



Il se déroule de la façon suivante :

* Le professeur principal ou un représentant de l'équipe pédagogique présente un bilan général de la classe. Il expose ensuite les conseils  en orientation formulés par l'équipe éducative.
* Le CPE fait le point sur la situation relative à l'absentéisme ou à la vie scolaire.
* Les délégués des parents d'élèves et les délégués de classe interviennent sur tous les aspects de la vie de la classe et les questions pédagogiques.
* Ensuite, le conseil de classe examine le déroulement de la scolarité de chaque  élève (résultats et appréciations des professeurs pour toutes les  matières, besoin d'accompagnement etc.).
* Les délégués notent toutes les informations concernant chaque élève. Ils peuvent intervenir pour soutenir un élève.
* Les délégués de classe et les délégués des parents d'élèves  restent dans  la salle lorsque l'on évoque leur scolarité ou celle de leur enfant.



 Rôle du conseil de classe :

* Le conseil de classe se met d'accord sur une appréciation générale  qui sera inscrite sur le bilan périodique (au collège) et sur le  bulletin trimestriel (au lycée). Il peut décerner une mention inscrite  au règlement intérieur (par exemple : *encouragements*, *félicitations*).

* Le conseil de classe est une instance pédagogique. Ce n'est pas une instance disciplinaire. Il ne peut donc pas prononcer de sanction.

* Le conseil de classe fait des _propositions_ d'orientation que les parents décideront ou non de suivre.



## CHS : La commission hygiène et sécurité 



Les compétences de la CHS  s’étendent à tout ce qui a trait à la sécurité et à  l’hygiène : équipements, machines, locaux, plan de sécurité en cas de  travaux, programme de formation et prévention des risques, suivi des  registres (registre d’hygiène et de sécurité, de signalement d’un  danger, document unique), suivi des visites de l’inspection du travail,  de la commission d’accessibilité (CCDSA), en particulier. 

En cette période trouble, elle est particulièrement importante et écoutée.

La commission  peut aussi faire des propositions visant le bien-être au travail,  l’aménagement des postes de travail, l’amélioration des conditions de  travail dans l’établissement.

 Les collègues volontaires pour participer à cette commission ont  particulièrement une responsabilité d’alerte et de signalement, mais  tout personnel peut aussi agir dans ces domaines.



Elle est composée par :

*  Le chef d’établissement, président.
*  Le gestionnaire.
*  Le CPE siégeant au CA.
*  Un représentant de la collectivité territoriale de rattachement.
*  Deux représentants du personnel au titre des personnels enseignants.
*  Un représentant au titre des personnels administratifs, sociaux, de santé, techniques, ouvriers de service. Ce nombre est porté à deux dans les établissements de plus de 600 élèves.
*  Deux représentants des parents d’élèves désignés au sein du conseil d’administration par les représentants des parents qui y siègent.
* Deux représentants des élèves désignés au sein du conseil des délégués des élèves par ces derniers.



## Le CESC :  **C**omité d'**E**ducation pour la **S**anté et la **C**itoyenneté.



C'est une instance de réflexion, d'observation et de proposition qui conçoit, met en œuvre et évalue un projet éducatif en matière  d'éducation à la citoyenneté et à la santé et de prévention de la  violence, intégré au projet d'établissement. 



Le CESC est présidé par le chef d'établissement et peut comprendre :

- des représentants des personnels enseignants, des parents et des  élèves désignés par le chef d'établissement sur proposition des membres  du conseil d'administration appartenant à leurs catégories respectives ;
- des personnels d'éducation, sociaux et de santé de l'établissement ;
- des représentants de la commune et de la collectivité de rattachement au sein de ce conseil ;
- des représentants des partenaires institutionnels (police,  gendarmerie, service départemental d'incendie et de secours (SDIS) et  associatifs) et un ou plusieurs représentants de la Réserve citoyenne de l'éducation nationale.



Ses missions sont :

* contribuer à l['éducation à la citoyenneté](https://eduscol.education.fr/D0090/CITOYACC.htm) ;

* préparer le plan de [prévention de la violence ](https://eduscol.education.fr/cid46847/prevenir-violence.html);

* proposer des actions pour aider les parents en difficulté et lutter contre l'exclusion ;

* définir un programme d'[éducation à la santé et à la sexualité ](https://eduscol.education.fr/pid23366/education-a-la-sexualite.html)et de [prévention des conduites à risques](https://eduscol.education.fr/cid46870/la-prevention-des-conduites-addictives-milieu-scolaire.html)

## La commission éducative 



Dans chaque collège ou lycée, la commission éducative examine la  situation d'un élève  qui ne respecte pas ses obligations scolaires ou  qui a un comportement inadapté.  La commission  propose alors des  mesures éducatives.

La composition de la commission éducative est fixée par le conseil d'administration. Elle est inscrite dans le [règlement intérieur de l'établissement](reglement_interieur.md). Chaque membre doit garder secret les faits dont il a connaissance pendant les réunions de la commission.

Elle est :

* présidée par le chef d’établissement ou son représentant qui peut y inviter toutes les personnes qu'il juge  utiles à l'examen du dossier.

* composée des personnels de l'établissement (dont au moins un enseignant), et au moins un parent d'élève.

    

La commission examine la situation d'un élève qui ne respecte pas ses obligations scolaires ou qui a un comportement inadapté.

Le responsable de l'élève est informé de la procédure et peut être, à sa demande, entendu par la commission.

La commission ne sanctionne pas le comportement de l'élève. Elle recherche une solution éducative adaptée et personnalisée à la situation, comme  par exemple la mise en place d'une mesure de responsabilisation. Elle cherche ainsi à amener l'élève à s'interroger sur sa conduite et sur les conséquences de ses actes.

La commission éducative est également consultée quand un incident implique plusieurs élèves.

Elle assure le suivi des solutions éducatives personnalisées mises en place.

Elle participe également à la prévention et à la lutte contre le harcèlement et la discrimination en milieu scolaire.





## Le conseil de discipline

Dégradations, comportement violent, harcèlement, absences aux cours  régulières et non justifiées... lorsqu’un élève est soupçonné d’avoir  commis une faute grave prévue dans le [règlement intérieur](reglement_interieur.md) d'un l'établissement, le conseil de discipline  peut se réunir à la demande de l’équipe éducative ou du chef  d’établissement.



L’élève et ses parents (ou ses représentants légaux) sont convoqués par  lettre recommandée. S’ils le souhaitent, ils peuvent se faire assister  par un professeur, un camarade de classe, un avocat... En fonction de la gravité de la faute, l'élève peut éventuellement se voir interdire  l’accès à l'établissement, pendant un maximum de 3 jours. C'est ce qu'on appelle une "mesure conservatoire".



Présidé par le chef d'établissement, le conseil de discipline est composé de :

*  Le proviseur adjoint

- le gestionnaire 
- le CPE
- cinq représentants des personnels 
- cinq représentants des usagers (trois parents et les deux délégués  de la classe en collège, deux parents et trois élèves en lycée).



Le conseil de discipline peut commencer dès que le nombre minimal de membres présents exigé est atteint (le quorum). Si le "quorum" n'est pas atteint, la réunion doit être reportée au minimum de 8 jours et au  maximum de 15.

Le rapport préalablement rédigé par le chef  d'établissement est tout d'abord lu devant les membres du conseil de  discipline, l'élève, son représentant légal et l’éventuel défenseur.

Il est procédé ensuite à l’audition des témoins, de deux professeurs de la classe ainsi que des deux délégués élèves. Lors du débat  contradictoire, l’élève et sa famille peuvent s’exprimer et exposer  leurs points de vue.

À l'issue des échanges, les membres du  conseil de discipline discutent entre eux puis prennent une décision.  Celle-ci est enfin annoncée à l'élève et à sa famille.

La décision prise par le conseil de discipline est **applicable même en l'absence de l'élève** et/ou de ses représentants légaux, dès lors que ceux-ci ont été convoqués dans les formes et les délais fixés par les textes.



La sanction prononcée dépend de la **gravité des faits** :  avertissement, blâme (violation répétée du règlement intérieur),  exclusion temporaire de la classe ou de l’établissement, exclusion  définitive. Le conseil peut décider que l’élève a obligation ou non  (c’est ce qu’on appelle un sursis) d’exécuter l’intégralité ou une  partie de la sanction. L'élève peut se voir également proposer des  mesures alternatives à la sanction, afin de le responsabiliser et de  prévenir d’autres manquements au règlement.







## Le conseil des délégués pour la vie Lycéenne (CVL)





Le conseil des délégués pour la vie lycéenne (CVL) est une instance où les lycéens sont associés aux décisions de leur établissement scolaire. Le  CVL est compétent pour débattre des questions sur le travail scolaire et les conditions de vie des élèves dans les lycées. Le conseil  d'administration de l'établissement scolaire le consulte quand il doit  traiter d'un de ces sujets.

Le CVL est composé de :

- 10 représentants des élèves (délégués de classe)
- 8 représentants de personnels travaillant au lycée,
- 2 représentants de parents d'élèves.

​	



_____________

Par Mieszczak Christophe

Licence CC BY SA

sources :

* _www.onisep.fr_
* _www.service-public.fr_

