# Organigramme d'un lycée





Vous êtes déjà allé au lycée non ? Tout le monde connaît non ? Il y a un proviseur, des CPE , des profs ... 



Oui...



Mais pas que et, surtout, quel est la fonction de chacun ?



> * Mettez vous en groupe, si possible ...
> * Réfléchissez aux différents personnels qui oeuvrent dans un lycée .
> * Enumérez les missions de chacun des personnels (administratif ou pédagogique).
> * Dessiner un organigramme reprenant tout cela.


![organigramme et missions](media/organigramme.jpg)







## Un travail d'équipe



Vous allez être professeurs d'informatiques.



> 
>
> * Quelles problématiques particulières sont liés à cet enseignement ?
> * Avec quels personnels particuliers allez vous travailler et pourquoi ?
>
> 





__________

Par Mieszczak Christophe

Licence CC BY SA

source image : production personnelle