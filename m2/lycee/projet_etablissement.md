# Projet d'établissement



## Utilité



Le projet d’établissement d'un collège ou d'un lycée définit les conditions dans lesquelles les programmes éducatifs nationaux sont mis en œuvre dans l'établissement. Il doit notamment préciser les activités scolaires et périscolaires organisées dans l'établissement.

Le projet d'établissement indique également les moyens mis en œuvre pour assurer la réussite de tous les élèves et associer les parents à ces  objectifs.

Il peut aussi prévoir la mise en place  d'expérimentations, pendant 5 ans au plus et avec l'accord des autorités académiques. Ces expérimentations peuvent  notamment concerner les sujets suivants :

- Enseignement de disciplines

- Organisation pédagogique de la classe ou de l'établissement (par exemple, "cours le  matin, sport l'après-midi" ou les "classes sans notes")

- Échanges ou jumelages avec des écoles étrangères

    

## Élaboration

Le projet d'établissement est élaboré, à l'initiative du chef d'établissement, par les différents membres de la communauté éducative.

Le [conseil pédagogique](instances.md)  prépare la partie pédagogique du projet.

Les partenaires extérieurs à l'établissement (entreprises, associations  culturelles, centres d'orientation et d'information, autres  établissements scolaires,etc.) peuvent aussi être consultés.





## Adoption et présentation

Le projet d'établissement est adopté par le [conseil d'administration](instances.md), pour une durée comprise entre 3 et 5 ans.

Il est présenté aux parents par la direction de l'établissement au moment de la première inscription de l'élève (lors d'une réunion ou par la remise d'un document).





## Un exemple de projet d'établissement



Voici [un exemple de projet d'établissement](projet_etablissement_2019_2023.pdf) .



* Quelles sont les Missions de l'école ?

    

* Quels sont les axes prioritaires du projet d'établissement ?

    

* Axe no 1 : 

    * Où et comment l'enseignement de l'informatique peut-il s'insérer ?
    * Quelle est la place de la formation continue des enseignants ?
    * Comment pourriez-vous participer à l'harmonisation des pratiques pédagogiques ?
    * Comment participer au "parcours avenir" ?

    

* Axe No 2 :

    * Comment pourriez-vous valoriser le travail des élèves ? 

    * Quelle particularités de la spécialité informatique devez-vous présenter au parents ?

        

* Axe No 3 : 

    * Quel lien pourriez-vous faire entre vos pratiques en informatiques et le développement durable ?

    * Quelle instance du lycée intervient tout particulièrement dans cet axe ?

        

* Quel indicateur apparaît dans chacun des axes ? De quoi s'agit-il ?

    

## Le projet de votre établissement



Refaites le même travail à partir du projet de votre établissement.





____________

Par Mieszczak Christophe

Licence CC BY SA

sources _service-public.fr_

