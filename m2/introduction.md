## Master 2 : introduction



Trois points importants sont identifiés pour la formation :

* "Créer un climat favorables aux apprentissages"
* "Proposer des contenus d'enseignements pertinents"
* "Favoriser la réussite de tous"



## Créer un climat favorable d'apprentissage



Quelles _postures_ pédagogiques à adopter, à différents moments clés du déroulement d'un cours, ou même d'une séquence de cours, afin de  :

* créer un environnement favorable au travail.

* mettre rapidement les élèves au travail.

* créer les meilleures conditions possibles d'apprentissage

    

Il s’agira de repérer les périodes critiques de la leçon (début de cours, changement d‘ exercice, utilisation de types de médias, fin de cours, etc..) et de questionner les organisations pédagogiques et les procédures d’intervention. 

L’enjeu est que l’étudiant puisse repérer les difficultés qu’il rencontre et identifie leurs natures (pédagogiques, didactiques, postures, ...). 

L’aide à la préparation de leçons et aux bilans seront des outils centraux pour faire émerger les premières transformations professionnelles à opérer pour organiser le travail en classe. 





## Proposer des contenus d'enseignements



Les interventions  seront centrées autour des questions suivantes :

* Comment choisir les contenus au regard du niveau des élèves et des exigences du programme ? 
* Quels types de travail proposer en fonction de la progression des élèves ? 
* Comment aborder une nouvelle notion ? 
* Comment renforcer un apprentissage nouveau ?

Ces questions que tout jeune enseignant se pose devront être le point de départ de cette seconde période de formation en UE 5. L’analyse des problèmes posés par la compétence visée, l’analyse des différentes situations éducatives proposées pour aider les élèves à résoudre ces problèmes, et l’analyse de l’activité de l’élève face aux tâches seront au cœur des suivis de stage UE 5.

Cette période de formation devrait permettre à l’étudiant de progresser dans ses préparations de leçons en mettant en relation : les compétences attendues, les  connaissances à mobiliser par ses élèves, et les dispositifs pédagogiques aidant les élèves à les mobiliser.



## Favoriser la réussite de tous



La thématique de **l’hétérogénéité** des ressources des élèves et les stratégies à mettre en place pour faire en sorte que tous les élèves apprennent seront l’objet central de cette période de formation. Ceci tout particulièrement après la période de confinement.



Les cours en suivi ainsi que les interventions du tuteur de terrain devront permettre de faire émerger des pistes pédagogiques (gestion de groupes, outils numériques ... ) et didactiques (progressivité des apprentissages à différentes vitesses ...) pour faire accéder l’ensemble de la classe aux compétences attendues par les programmes.



___________

Par Mieszczak Christophe

Licence CC BY SA