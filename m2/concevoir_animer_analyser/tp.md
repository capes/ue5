# Préparer un TP

L'informatique se prête particulièrement bien aux TP.

* Il peut d'agir d'une façon de faire cours souvent plus efficace et attrayante qu'un cours magistral. L'élève découvre alors les notions au fur et à mesure qu'il en a besoin et les appliquent dans sur des cas concrets, parce qu'il en a besoin.
    * Il peut s'agir d'un apprentissage aux projets où les élèves réalisent un _projet plus ou moins guidé_ afin d'approfondir leur maîtrise d'une notion en travaillant en équipe, si possible.



Voici [un exemple de TP/cours](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/representations/representation_des_caracteres/codages_des_caracteres.md)  autour du codage des caractères qui permet de découvrir le cours, avec un peu de classe inversée (ou ça ??) en travaillant en HTML et CSS.



Voici [un autre exemple de TP](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/projets/nim/nim.md) , autour du jeu de Nim ,servant à solidifier et approfondir les notions apprises en programmation : 

* définitions de fonctions
* conditions
* boucles
* variables et opérateurs.



>  
>
> On choisit une séquence qui se prête à ce genre de posture et en avant !
>
> 



__________

Par Mieszczak Christophe

licence CC BY SA