# Préparer une progression



## LA base



Il existe différentes façon de transmettre un savoir. Le choix du _comment enseigner_ s'appelle la **liberté pédagogique**. 

Cependant le savoir en question nous est imposé. Il est inscrit dans le **B**ulletin**O**fficiel. 

* [Celui de première NSI](BO/BO_premiere.pdf)
* [Celui de terminale NSI](BO/BO_terminale.pdf)
* Et aussi celui de [seconde en SNT](BO/BO_snt.pdf) car vous y interviendrez certainement.



Dans la plupart des disciplines, en particuliers en Mathématiques, il existe des grilles de compétences spécifiques à la discipline.

Ce n'est pas encore le cas en NSI et SNT, pour l'instant.

En revanche, il existe un outils pour valider des compétences générales en informatiques : [PIX](https://pix.fr/).

 Allez y jeter un coup d'oeil, nous y reviendrons au moment de l'[évaluation](concevoir_animer_analyser/evaluation.md) ...





## BO et progression



Ouvrons le BO de première.



### Contenus



> * Quels sont les concepts fondamentaux du programme ?
>     * données
>     * algo
>     * langages
>     * machines
> * Rangez les huit rubriques dans ces concepts.
> * 
> * Quelles rubriques sont indépendantes des autres ? 
> * Quelles sont les rubriques inter-dépendantes ?  



### Certes, mais les prérequis ?



Et oui, il y a une vie avant la première. Les élèves sont allés en SNT et sont _sensés_ avoir acquis certaines bases, notamment en Python.

Sauf que ce n'est pas toujours le cas... Surtout cette année.

Il faut donc, si on ne veut pas noyer tout le monde avant le mois d'octobre, prendre cela en compte.



> 
>
> * Quelles compétences, importantes pour nos contenus, un élèves est sensé avoir en entrant en première ?
> * Quelles rubriques permettrait de revoir ces compétences pou démarrer l'année doucement et sereinement ?
>
> 



### Seul au monde ?



Une particularité de la NSI et que, sauf cas exceptionnel, vous serez le seul membre de votre équipe pédagogique. 

Cependant :

* Vous n'êtes pas le seul à faire de l'informatique au lycée :
    * en mathématiques, en première et terminale, le programme en Python est copieux.
    * Dans une moindre mesure, Python intervient également en Science Physiques et Science et Vie de la Terre.
    * Si votre lycée propose l'option Science de l'Ingénieur, le ou les professeurs intervenants pourront travailler avec vous.
    * Le grand oral, en terminale, demande de répondre à deux questions portant sur les spécialités suivies par l'élèves : s'intéresser aux autres spécialités est donc nécessaire.
    * Dans un lycée général et technologique, il y a, en STI2D, une autre filière informatique : la SIN (sciences informatiques et numériques). S'entendre avec les personnes y intervenant permettra peut-être de partager certaines choses (matériel, salle, TP ...)
    * En SNT, des enseignants de diverses matières peuvent intervenir. Vous en particulier, spécialistes de la discipline. Organiser une progression commune et un travail d'équipe sur ce niveau est crucial.



> Renseignez vous pour savoir ce que les enseignants en Mathématiques, Science Physique, SVT et SI font en informatique. Comment pourriez vous mener un projet inter-disciplinaire ?
>
> * Cherchez dans les BO
> * Parlez avec des enseignants de ces disciplines !







### Une progression

Il n'existe pas d'organisation parfaite. Elle dépend de multiples facteurs de vos élèves, de votre établissement, de votre personnalité, de votre recul par rapport au programme... Un professeur très aguerri pourra, par exemple, envisager une progression par projets où les morceaux du puzzle des contenus se complètent au fur et à mesure. Un professeur débutant préférera une progression par chapitre ou par morceaux de chapitre plus simple à organiser et à gérer.



Proposez une progression d'apprentissage des contenus :



* Ne pas rester trop longtemps dans une même rubrique :
    * les élèves se lassent vite. Ils zappent. Il faut zapper aussi.
    * Les élève oublient : il est bien plus pertinent de revenir plusieurs fois sur une partie que de tout faire d'un coup et ne plus en parler.
* Attention aux pré-requis.
* Il est possible de mener deux chapitres en parallèle (par exemple 2h / 2h) à certains moment.
* Prenez en compte les spécificités du lycée où vous menez votre stage :
    * combien de temps êtes vous en demi-groupe ? en classe entière ? en salle informatique ?
    * vous disposez de 2h consécutives ou d'une seule ?
    * Quand pouvez vous disposer de matériel  (micro:bit, arduino, rasberry ...)



Essayez maintenant de placer, sur cette progression, des marqueurs temporels : combien de temps passer sur chaque partie ?

Affiner afin de tenir compte des impératifs du calendrier :

* Les épreuves de BAC, pour ceux qui décideront d'arrêter la NSI , arrivent début mai.
* Mi juin, au plus tard, le programme doit être bouclé.



Ces marqueurs ne seront peut-être (sûrement) pas respectés mais ils vous serviront l'année prochaine, quand il faudra repenser la progression.



### Prenez des notes



Votre progression est prête. Elle n'est pas parfaite. Vous  allez vous en rendre compte pendant l'année : telle ou telle chose aurait dû être faite avant ou après, à certains moments, la fatigue des élèves les ont empêchés de se concentrer sur une notion ardue, vous avez de nouvelles idées, un nouveau matériel ....



Une chose est certaine, dans un an, cette idée ou remarque, vous l'aurez oubliée : Il faut la noter !! 



Ainsi, pensez à commenter votre progression au fur et à mesure que vous la vivez vous fera gagner beaucoup de temps par la suite !



## Progressions travaillées par les étudiants :



* [Florient Mathieu et ???](progressions_etudiants/progression_1.md)

* [un gros travail de  Philippe Boddaert ](progressions_etudiants/progression_2.md)

* [Nathan - Antoine - Philippe - Enzo - Younes](progressions_etudiants/progression_3.md)

    

    







___________

Par Mieszczak Christophe

Licence CC BY SA