# Progression 1ère NSI
Nathan - Antoine - Philippe - Enzo - Younes

Nous avons fait le choix de commencer directement par les base en python afin de l'utiliser comme un outil pour les prochains chapitres.
Au début de l'année, les chapitres sont établis dans un ordre qui permet d'alterner entre du "python pur" et des notions plus théoriques.

* ## Langage et programmation
  * 1er pas en python
  * Notion de variable
  * Affection de variable

* ## Représentation des données : types et valeurs de base
  * Ecriture d'un entier dans un base (2,10,16)
  * Entier relatif
  * Booléens

* ## Langage et programmation
  * Structures de contrôles
  * Boucle bornée et non bornée
  * Notions de fonction
    * TP conversion entre base

* ## Représentation des données : types et valeurs de base
  * Flottants
  * Texte

* ## Types construits
  * p-uplets
  * Liste
  * Dictionnaire

A partir de ce moment, les élèves ont acquis beaucoup de choses en python permettant que construire des algorithmes simples.

* ## Algorithme
  * Parcours séquentiel d'un tableau
  * Tri par insertion/sélection
  * Recherche dichotomique

* ## Architecture
  * Systèmes d'exploitations
  * IHM

* ## Interactions entre l'homme et la machine sur le web
  * WEB, HTML, CSS
  * Evénements

* ## Architecture
  * Transmission données dans un réseau
  * Architecture d'un réseau

* ## Interactions entre l'homme et la machine sur le web
  * Interactions client/serveur
  * Formulaire

* ## Traitement données en tables
  * Indexation
  * Recherche
  * Tri
  * Fusion

Le chapitre suivant a comme pré-requis les base de python ainsi que la lecture des données en tables. C'est pour cela qu'il arrive si tard, même si on pourrait le faire un peu plus tôt afin de bien aborder les notions nécessaires au QCM du bac.
* ## Algorithme
  * Algorithme des k plus proches voisins
  * Algorithme glouton

* ## Architecture
  * Von Neumann



___________

Nathan - Antoine - Philippe - Enzo - Younes