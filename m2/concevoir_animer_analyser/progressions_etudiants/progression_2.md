### Progression en 1ère

Ce document contient la progression de la spécialité NSI en première.

C'est un découpage du programme issu du B.O du point de vue des 4 trimestres de l'année scolaire.

## Description

| Trimestre | Séquence 1               | Séquence 2                 |
| --- | ------------------------ | -------------------------- |
| 1 | **Langage de programmation** :<br />  - Affectation<br />  - Structures conditionnelles<br />  - Fonctions<br />  - Boucles bornées et non bornées<br />  - Mutabilité | **Représentation des données : types et valeurs de base** : <br/>  - Numération<br />  - Écriture binaire des entiers relatifs<br />  - Écriture binaire des nombres à virgules flottants<br />  - Conversion de base (binaire, hexadécimal, décimal)<br /><br />**Représentation des données : types construits** : <br />  - List<br />  - Tuple<br />  - Dictionnaire |
| 2 | **Algorithmique** : <br />  - Terminaison<br />  - Complexité<br />  - Recherche de minimum <br />  - Tri par sélection<br />  - Insertion dans une liste triée<br />  - Tri par insertion <br/> | **Représentation des données : types et valeurs de base** :<br />  - Encodage des caractères<br />  - Traitement des données en table (CSV)<br /><br />**Architectures matérielles et systèmes d'exploitation** : <br />  - Définition d'un OS<br />  - Linux et commandes |
| 3 | **Algorithmique** :<br />  - Dichotomie<br />  - Algorithme Glouton | **Réseau et web**<br />  - Pages Web statiques<br />  - Interactions utilisateur-client (événements en JS)<br />  - Interactions client-serveur (requêtes GET, POST) |
| 4 | **Algorithmique** : <br />  - Introduction aux algorithmes d’apprentissage<br />  - K plus proches voisins | **Réseau et web**<br />  - Protocole IP et adressage<br/>  - Les modèles OSI et TCP/IP<br/>  - architecture et routage<br /><br />**Architectures matérielles et systèmes d'exploitation**<br />  - Modèle Von Neumann<br />  - Langage assembleur |

## Justification

Par le biais de cette progression, je souhaite travailler sur 2 plans en parallèle :

- un plan où l'élève agit (programmation python, html, lancement de commande...);
- un plan où l'élève est dans un niveau plus d'abstraction (activité débranchée, algorithmique)

Pour faire sens, à chaque trimestre, l'élève est amené à aller plus loin dans le détail de sa réflexion et de la connaissance d'une machine.

Je souhaite partir d'un langage de haut niveau (Python) pour aller vers un langage de bas niveau (assembleur).

De même, l'élève est placé au plus tôt de l'année devant un écran d'ordinateur, lui faire manipuler un IDE, et au fur et à mesure aller vers la connaissance du matériel et le réseau.

En résumé, elle peut être modélisée par le schéma suivant :

**Langages => Données => Algorithme => Matériel + Réseau**

## Plus de détails

J'ai détaillé et ai découpé ma progression en activités dans le tableau suivant :

<table>
  <thead>
    <tr style="height:20px;">
      <th>Trimestre</th>
      <th>Leitmotiv</th>
      <th>Objectifs</th>
      <th>Activités</th>
      <th>Rubrique</th>
      <th>Contenus</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td rowspan="14">1er</td>
      <td rowspan="14">- Qu'est-ce que coder et programmer ?<br>- Qu'est-ce qu'une donnée en machine ?</td>
      <td rowspan="14">- l'élève connait et maitrise les instructions élémentaires Python<br>- l'élève sait lire, écrire, modifier un algorithme<br>- l'élève sait représenter des types de données en binaire</td>
      <td>Cours</td>
      <td></td>
      <td>- Présentation de la matière NSI<br>- Présentation de l'organisation (outils, découpage...)<br>- Formulaire connaissance des élèves</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Langages et programmation</td>
      <td>- Langage de programmation (histoire, définition, distinction interprété / compilé...)</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Langages et programmation</td>
      <td>- Mise en place de python et IDE<br>- Manipulation des variables (type élémentaires : int, str, bool)<br>- Execution d'instructions (opérateurs arithmétiques, fonction de base)</td>
    </tr>
    <tr>
      <td>Débranché</td>
      <td>Algorithmique</td>
      <td>- Qu'est-ce qu'un algorithme ?</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Algorithmique</td>
      <td>- Histoire des algorithmes<br>- Pseudo-code (boucles, etc.)</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Langages et programmation</td>
      <td>- Structures conditionnelles (if, if … else, etc.)<br>- Opérateurs de comparaison (==, !=, &lt;, &lt;=, etc.)</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Algorithmique</td>
      <td>- Algèbre de Boole (tables de vérité)<br>- Opérateurs logiques (not, and, or, etc.)</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Langages et programmation</td>
      <td>- Fonctions (docstrings, spécification, prédicats, etc.)<br>- Fonctions pour parcourir (range, enumerate, etc.)<br>- Modules (help, import, https://pypi.org/, etc.)</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Représentation des données : types et valeurs de base</td>
      <td>- Numération positionnelle</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Représentation des données : types construits</td>
      <td>- Types construits (tuple, list, dict, in, etc.)<br>- Boucles bornées</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Langages et programmation</td>
      <td>- Boucles non bornées<br>- Boucles sur les types itérables (for x in l, etc.)<br>- Mutabilité et pointeurs avec Python Tutor</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Représentation des données : types et valeurs de base</td>
      <td>- Écriture binaire des entiers naturels et relatifs</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Représentation des données : types et valeurs de base</td>
      <td>- Problématique des réels<br>- Norme IEEE 754</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Langages et programmation</td>
      <td>- Conversion de bases</td>
    </tr>
    <tr>
      <td rowspan="14">2ème</td>
      <td rowspan="14">- Qu'est-ce que prouver ses algorithmes ?<br>- Qu'est-ce qu'un système d'exploitation ?</td>
      <td rowspan="14">- l'élève sait déterminer si un algorithme termine<br>- l'élève sait déterminer si un algorithme est correcte<br>- l'élève connait le fonctionnement d'une système d'exploitation<br>- l'élève sait intéragir avec un OS l'aide de commandes et instructions bas niveau</td>
      <td>TP</td>
      <td>Représentation des données : types et valeurs de base</td>
      <td>Encodage des caractères<br>- ASCII<br>- ISO-8859-1<br>- Unicode</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Langages et programmation</td>
      <td>- Fonction de lecture et écriture de fichiers texte</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Langages et programmation</td>
      <td>- Traitement des données en table (fichier CSV)</td>
    </tr>
    <tr>
      <td>Projet</td>
      <td></td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Algorithmique</td>
      <td>- Preuve de terminaison (variant)<br />- Preuve de correction (invariant)</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Algorithmique</td>
      <td>- Définition de la complexité<br />- Classes de complexité</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Architectures matérielles et systèmes d'exploitation</td>
      <td>- Définition d'un OS</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Architectures matérielles et systèmes d'exploitation</td>
      <td>- Linux et lignes de commandes</td>
    </tr>
    <tr>
      <td>Débranché</td>
      <td>Algorithmique</td>
      <td>- Les tris</td>
    </tr>
    <tr>
      <td>Évaluation</td>
      <td></td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Algorithmique</td>
      <td>- Recherche du minimum<br />- Tri par sélection<br />- Correction / Complexité</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Algorithmique</td>
      <td>- Insertion dans une liste triée<br />- Tri par Insertion<br />- Correction / Complexité</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Algorithmique</td>
      <td>- Les tris<br />- Mesure de complexité</td>
    </tr>
    <tr>
      <td>Projet</td>
      <td></td>
    </tr>
    <tr>
      <td rowspan="14">3ème</td>
      <td rowspan="14">- Que communique-t-on sur le WEB ?<br>- Comment échanger des données de manière secrète ?<br>- Que se pase-t-il quand on cherche LA meilleure solution ?</td>
      <td rowspan="14">- l'élève sait distinguer problème de décision et d'optimisation<br>- l'élève sait créer une page web dynamique<br>- l'élève sait utiliser un mécanisme de chiffrement / déchiffrement</td>
      <td>Cours</td>
      <td>Réseau et Web</td>
      <td>- Histoire du Web<br>- Architecture Client / Serveur<br>- Protocole HTTP</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Réseau et Web</td>
      <td>- Pages Web statiques (HTML + CSS)</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Algorithmique</td>
      <td>- Recherches séquentielle et dichotomique</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Algorithmique</td>
      <td>- Résolution de problème avec stratégie dichotomie</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Réseau et Web</td>
      <td>- Interactions utilisateur-client (événements en JS)</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Réseau et Web</td>
      <td>- Interactions client-serveur (requêtes GET, POST)</td>
    </tr>
    <tr>
      <td>Projet</td>
      <td></td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Algorithmique</td>
      <td>- Introduction aux problèmes d’optimisation<br />- Algo glouton : Problème du rendu de monnaie</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Algorithmique</td>
      <td>- Algo glouton : Problème du sac à dos</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Algorithmique</td>
      <td>- Algo glouton : Problème d’organisation</td>
    </tr>
    <tr>
      <td>Débranché</td>
      <td>Algorithmique</td>
      <td>- Cryptographie</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Algorithmique</td>
      <td>- Chiffrement symétrique et asymétrique</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Algorithmique</td>
      <td>- Chiffrement césar, vigenère</td>
    </tr>
    <tr>
      <td>Cours / TP</td>
      <td>Algorithmique</td>
      <td>- Chiffrement des communications (HTTPS)</td>
    </tr>
    <tr>
      <td rowspan="11">4ème</td>
      <td rowspan="11">- Qu'est-ce qu'une machine ?<br>- Comment faire apprendre à une machine ?<br>- Qu'est-ce qu'un réseau ?</td>
      <td rowspan="11">- l'élève connait l'architecture d'une machine et intéragit avec elle à l'aide d'instructions de base<br>- l'élève connait la notion d'algorithme d'apprentissage<br>- l'élève connait l'organisation d'un réseau et le configurer</td>
      <td>Débranché</td>
      <td>Architectures matérielles et systèmes d'exploitation</td>
      <td class="s8">- Processeurs M10 et M999</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Architectures matérielles et systèmes d'exploitation</td>
      <td>- Modèle Von Neumann</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Architectures matérielles et systèmes d'exploitation</td>
      <td>- Programmation assembleur</td>
    </tr>
    <tr>
      <td>Projet</td>
      <td></td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Algorithmique</td>
      <td>- Introduction aux algorithmes d’apprentissage<br />- K plus proches voisins : notions de distance, classe</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Algorithmique</td>
      <td>- Naufragés du Titanic : Auriez-vous été rescapé ?</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Algorithmique</td>
      <td>- Morphologie d'un sportif : Quel sportif êtes-vous ?</td>
    </tr>
    <tr>
      <td>Débranché</td>
      <td>Réseau et Web</td>
      <td>- Protocole IP et adressage</td>
    </tr>
    <tr>
      <td>Cours</td>
      <td>Réseau et Web</td>
      <td>- Les modèles OSI et TCP/IP<br />- architecture et routage</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Réseau et Web</td>
      <td>- Etude de l'encapsulation avec Wireshark</td>
    </tr>
    <tr>
      <td>TP</td>
      <td>Réseau et Web</td>
      <td>- Simulation avec Packet Tracer</td>
    </tr>
  </tbody>
</table>


___________

Par Philippe Boddaert 