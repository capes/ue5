# Progression 1ère NSI

* ## Système d'exploitation
  * Histoire des OS       
  * Libre / Propriétaire
  * Ligne de commande 


* ## Construction élèmentaire
  * Variables
  * Affectations
  * Opérations & Comparaisons
  * Structures conditionnelles
  * Boucles bornées / Non bornées

* ## Architecture
  * Histoire de l'informatique matérielle
  * Von Neumann
  * Portes logiques
  
* ## Valeurs booléennes
  * Booléens, algèbre de Boole
  * Circuits combinatoires

* ## Représentation des nombres
  * Nombres naturels
  * Nombres relatifs
  * Nombres décimaux (codage approximatif -> flottants)
  * Bases 
  
* ## Spécifications et fonctions
  * Fonctions
  * Doctests & Docstrings
  * Assertions
  * Jeux de test
  
* ## Utilisations d'une bibliothèque
  * Bibliothèque


* ## Tableaux - Listes
  * Fonctionnement
  * Manipulation

* ## Chaines et encodage
  * Tableaux de caractères en lecture seule
  * Ascii & Unicode

* ## Tuples et dictionnaires
  * Tableaux particuliers
  * Dictionnaire : tableau par clé

* ## Réseau
  * TCP / IP
  * Modèle OSI
  * Matériel
  * Encapsulation

* ## Web
  * HTTP
  * Client - Serveur
  * Post - Get
  * Javascript
  
* ## Algorithmique et tris
  * Parcours séquentiel d'un tableau
  * Tri par selection
  * Tri par insertion
  
* ## CSV
  
* ## Algorithmes des k plus proches
  * Prédiction en algorithme
  * Algo Glouton et choix local
  



_____________

Florient Mathieu et ???

