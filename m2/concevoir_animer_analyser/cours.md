# Préparer un cours magistral

Il s'agit ici de préparer un cours magistral.

Il doit être clair, faire participer les élèves en prenant en compte l'hétérogénéité (exemples de difficultés bien dosées, prise de parole des élèves), varier les supports (avec une vidéo par exemple) et ne pas durer trop longtemps. 

La posture magistrale n'est pas la préférée des élèves. Il faut donc l'utiliser lorsque cela est nécessaire pour des notions particulières. Si votre présence n'est pas indispensable, une classe inversée ou un TP fera mieux l'affaire.



Voici un [exemple de cours magistral](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/architecture/microprocesseurs/architecture_van_neuman.md) :

* Il n'est pas question de tout faire d'un bloc !
* Notez les parties où les élèves pourront intervenir 



> 
>
> 
>
> Choisissez une partie d'un chapitre nécessitant cette posture et en avant !
>
> 
>
> 





__________

Par Mieszczak Christophe

licence CC BY SA