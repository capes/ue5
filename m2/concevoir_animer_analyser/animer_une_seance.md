# Organiser une séance



## Bien démarrer

Les premières minutes d'une séance sont cruciales.



Il faut réaliser, en perdant un minimum de temps, une succession d'actions :

* Accueil des élèves avant qu'ils n'entrent dans la classe. En situation COVID, surtout en salle informatique, rappeler les consignes et les faire se laver les mains (et laver les vôtres).
* Installation des élèves et mise en condition d'écoute et de travail.
* L' appel : une obligation légale qui permet également de récolter des informations auprès des élèves et, en début d'année, de faire connaissance.
* S'il y a un travail personnel (travail maison) en cours, prendre des nouvelles de son avancement, répondre à d'éventuelles questions
* Le rappel du cours précédent : indispensable, il aide également à remettre les élèves en situation d'écoute
* Enfin, on démarre la nouvelle séance.



## Différentes postures



### Le cours magistral



Il s'agit d'une stratégie dite _descendante_ :

* Le professeur expose devant la classe un cours clair et structuré.
* Les élèves écoutent, posent des questions, éventuellement prennent des notes.
* Après le cours, le professeur organise des activités de difficultés et de genre variés adapté à l'hétérogénéité de la classe



> * Quels sont les avantages de ce genre de séquence ? 
> * Quels sont ses inconvénients ?
> * Sous quelles conditions (contenu / public/ taille du groupe) peut-on ou doit-on l'utiliser ?
> * Comment rendre un cours magistrale plus attrayant pour les élèves ?



>Voici le [BO de première](BO/BO_premiere.pdf)
>
>* Quelles parties du programme se prêtent à un cours magistral (il peut s'agir d'une partie d'un chapitre uniquement) ?
>* Quelles parties du programme ne s'y prêtent pas ?
>
>



### Classe inversée



Le principe est de fournir aux élèves un cours à travailler en autonomie chez eux tandis que les activités seront faites en classe, le cours étant supposé travaillé.



Les activités en classe pourront :

* introduire/découvrir les notions qu'ils auront à étudier à la maison
* leur permettre de rendre ces notions plus concrètes.
* approfondir le cours au travers d'activités plus pointues.



Il faudra prévoir une remédiation pou ceux qui ne pourront être assez autonome pour travailler suffisamment le cours à la maison et le comprendre.



> 
>
> * Quels sont les avantages de ce genre de séquence ? 
>
> * Quels sont ses inconvénients ?
>
> * Sous quelles conditions (contenu / public/ taille du groupe) peut-on ou doit-on l'utiliser ?
>
> * Quels moyens peut-on utiliser pour rendre le cours plus attrayant à la maison ou faciliter l'entre-aide ?
>
>   ​    
>
> 



>Voici le [BO de première](BO/BO_premiere.pdf)
>
>* Quelles parties du programme se prêtent à une classe inversée (il peut s'agir d'une partie d'un cours uniquement) ?
>* Quelles parties du programme ne s'y prêtent pas ?





### Travaux pratiques



Il s'agit d'utiliser des TP pour :

* Découvrir une notion.
* Comprendre l'intérêt d'en savoir plus à son sujet.
* permettre aux élèves (ou groupe) d'avancer à leur rythme en gérant l'hétérogénéité.



Les besoins en cours sont introduites :

* Lorsqu'un élève (ou groupe) en a besoin. Il est possible de le faire dans le TP lui même.
* Pas tous en même temps à la même vitesse et avec la même profondeur.



En fin de TP, un résumé de cours doit être fait, avec les élèves si possible afin qu'il conserve un trace fiable et complète.



> * Quels sont les avantages de ce genre de séquence ? 
> * Quels sont ses inconvénients ?
> * Sous quelles conditions (contenu / public/ taille du groupe) peut-on ou doit-on l'utiliser ?
> * Comment intégrer cours et TP ?



_Remarque :_

[France IOI](http://www.france-ioi.org/) propose un apprentissage par TP très intéressant, exploitable aussi bien en seconde, qu'en première et terminale.

Il est intéressant d'y jeter un oeil (ou les deux) et de l'utiliser de temps en temps.



>Voici le [BO de première](BO/BO_premiere.pdf)
>
>* Quelles parties du programme se prêtent aux TP (il peut s'agir d'une partie d'un cours uniquement) ?
>* Quelles parties du programme ne s'y prêtent pas ?
>
>





### Projets



Un projet ressemble  à un TP. Cependant, il n'est pas autant guidé et l'élève (ou le groupe) doit élaborer ses propres solutions pour atteindre son objectif. 



Comme pour un TP, un projet permet :

* Découvrir une notion.
* Comprendre l'intérêt d'en savoir plus à son sujet.
* permettre aux élèves (ou groupe) d'avancer à leur rythme en gérant l'hétérogénéité.



Les besoins en cours sont cependant u peu différents :

* Lorsqu'un élève (ou groupe) en a besoin selon la solution utilisée
* Pas tous en même temps à la même vitesse, avec la même profondeur, pas forcément exactement sur le même sujet.



De la même façon, à la fin du projet, une synthèse des notions abordées doit être réalisée, si possible avec les élèves,  afin qu'ils en conservent une trace claire.



> * Quels sont les avantages de ce genre de séquence ? 
> * Quels sont ses inconvénients ?
> * Sous quelles conditions (contenu / public/ taille du groupe) peut-on ou doit-on l'utiliser ?
> * Comment intégrer cours et projets ?
> * Quelle conséquence sur une progression ?





### Travaux de groupe



En dehors de cette période compliquée et _COVIDée_ où les règles de distanciation nous enlèvent toute possibilité de regroupement d'élèves,  ou presque, le travail de groupe a de multiples avantages. D'ailleurs, il reste possible d'organiser ces groupes sans rapprocher physiquement les élèves (en installant un tchat en salle info par exemple)



Côté élève : 

* lors de gros TP ou projets, ils doivent apprendre à se connaître, à s'organiser et à s'entre-aider, chacun portant la responsabilité du groupe.
* ils doivent élaborer une répartition des tâches à effectuer et donc les identifier avant de commencer à coder. Cette répartition peut également être utilisée pour gérer l'hétérogénéité de la classe de façon à ce que chacun puisse atteindre un objectif à sa portée.
* ils doivent utiliser des outils de communication et de partage données.



Côté enseignant :

* Gérer 7 ou 8 groupe est moins compliqué que de gérer 30 individus.
* Ramasser 7 ou 8 projets à corriger est moins chronophage que d'en ramasser 30.



Il y a aussi des inconvénients, comme chaque fois ...



Côté élèves, le risque est que le costaud du groupe fasse tout le travail et qu'à l'opposé, l'élève timide ou en difficulté (ou pas très _investi_) se repose sur les autres ou plombe le groupe.



Côté enseignant, l'évaluation de chaque membre dans le groupe suppose une bonne connaissance des individus et de leur travail pendant le projet. Il faudra également prévoir des systèmes pour vérifier le travail réel de chacun.



### Evaluation



Les évaluations font parties de l'enseignement. 

Le sujet est vaste, nous y consacreront une partie complète.



### Conclusion : tout mélanger



Vous l'avez compris, il n'y a pas de système ultime,meilleur que tous les autres. Tous sont intéressants et tous sont critiquables. 

L'idéal est probablement de bâtir ses cours en les utilisant tous à bon escient :

* Un peu de magistral est utile pour fixer clairement les notions importantes ou trop compliquées pour être abordées seul chez soi. 
* Pour les choses simples et mécaniques, la classe inversée peut être très utile. Pour éviter de _saouler_ les élèves avec, par exemple,  l'énumération des méthodes d'une liste ou d'un dictionnaire.
* Les TP permettent de guider les élèves vers la découverte et l'approfondissement d'une notion, de les mettre en situation de  _recherche_, de susciter leur curiosité.
* Les projets, lorsque les élèves sont assez aguerris, permettent une progression moins linéaire et différenciée.



Les élèves sont des zappeurs. Il faut donc varier sans cesse, éviter de rester trop longtemps sur une même notion dans votre progression mais également sur une même posture pendant une séance.





## Bien finir



Le cours est bientôt terminé, il ne reste que quelques minutes.

C'est le moment de :

* s'arrêter : il est inutile de commencer quelque chose maintenant.
* faire le point de ce qui a été ou pas.
* donner les consignes pour le prochain cours en expliquant brièvement ce qu'on y fera.
* éventuellement, évoquer une évaluation.
* REMPLIR LE CAHIER DE TEXTE : une obligation légale !







___________

Par Mieszczak Christophe

Licence CC BY SA