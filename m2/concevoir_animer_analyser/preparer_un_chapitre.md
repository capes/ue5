# Préparer un chapitre





Vous allez ici préparer des chapitres de votre progression de A à Z en l'adaptant en fonction de spécificités de vos élèves, de votre établissement.

Le format retenu est le Markdown.



Que ce soit pour le cours ou les TP/projet, vous préciserez à chaque fois :

* Quelles partie de la progression traitez-vous ?
* A quelles partie du BO cela correspond-t-il ?
* Quels sont les prérequis ?
* De quels outils informatiques disposez-vous (vidéo / machines / logiciels)
* Dans quelles conditions travaillez vous sur ce chapitre : classe entière ? demi-groupe ?



Une fois la séance préparée, vous la présenterez devant le groupe.

Une discussion s'établira où vous justifierez vos choix et répondrez aux questions diverses du public.



## Le cours 

* une partie cours magistrale lorsque la présence du professeur est indispensable et la notion difficile à faire passer en TP.

    * claire, pas trop long, avec des exemples simples. On pourra utiliser une ressource vidéo.

    * une _capsule_ spéciale confinement, à regarder chez soi, qui reprend le cours magistrale en version vidéo afin d'aider l'élève à comprendre malgré votre absence.

        * on pourra utiliser un logiciel de capture vidéo écran/audio comme _Kazam_.

        * la vidéo n'excédera pas 10 min.

            

* une partie en classe inversée. Il s'agit de gagner du temps lorsque la présence du professeur n'est pas nécessaire afin d'en avoir plus (du temps) pour pratiquer en classe. Il faudra tout de même prévoir un résumé de cette partie, résumé qui peut être réalisé par un élève (préparation au grand oral ...). Cela peut concerner :

    * une partie de cours simple que les élèves peuvent appréhender seuls.

    * un TP applicatif , voir mécanique, pour introduire une notion. 

        

* une partie TP/cours. Il s'agit ici de rédiger un TP guidé dans lequel les notions de cours apparaissent au fur et à mesure.

    * les élèves vont à leur rythme.
    * le professeur fait le point régulièrement et souligne les parties de cours.



Il y a de nombreuses variantes comme celle qui consiste à demander à un élève ou un groupe d'élèves particulièrement doués de préparer une partie du cours ou une capsule à présenter aux autres. 



## TP ou projet

Il ne s'agit plus d'introduire le cours comme précédemment mais de l'approfondir. 

Le TP peut se faire entièrement en classe, ou entièrement à la maison (DM), ou un mixte entre les deux. On peut prévoir un TP relativement court à faire en classe puis un autre en DM. Selon le volume de travail, les élèves peuvent travailler seuls ou en binôme.



Il peut s'agir :

* Un TP débranché ne nécessitant pas de matériel informatique.
* Un TP plus ou moins guidé, selon le niveau de la classe et l'avancement dans l'année, exploitant le matériel informatique disponible.



_Remarque :_

Le TP peut être noté (ou pas). Pour éviter les copier-coller dans un TP de programmation, il faut bien insister sur les remarques qui doivent être nombreuses et personnelles.



## Evaluation _s_



On y revient vite



## Les groupes

* Philippe, Younes, Antoine, Enzo : 

    * Type construit : les tableaux.

    * classe inversée : Antoine

        * définition

        * application 

            

    * Cours magistral : Enzo

        * listes/tableaux : différence.

        * liste par compréhension

        * python tutor : mutabilité

            

    * tp/cours : Younes

        * après le magistral ?
        * tableau de tableau 
        * algo / méthode à reprogrammer
        * copie d'une liste
        * notion de slice ... : hétérogénéïté

    * Capsule : antoine 5-6'

        * les bases : 
        * mutabilité : 

    * projet : philippe

        * jeu de la vie / wator / gestion bibliothèque

        * puissance 4

        * obfuscation  : site qui le fait -> 
        
          * du coup l'élève peut utiliser le produit fini pour tester et comprendre
          * 
          
            





  

* Jessy, Nathan , Floriant :

    * numération : binaire / hexa / base

    * trois Capsules :

      * basés sur ce pas sorcier 1996
      * [lien]()
      * vidéo1 -> binaire
      * video2 -> numération 
      * video3 -> portes logiques
      
    * Ajout d'un TP :

      * algo de base
      
      * implémentation d'un complément à 2
      
        
        
          
      
      ​    

* Mickael, Timothé :

    * Dictionnaires
    
*  TP / cours
   
    * TP -> pokémon
    
* 2ème TP :
  
* idée -> implémenter un jeu avec dictionnaire
  
  
  
  
  
    
  
    











__________

Par Mieszczak Christophe

Licence CC BY SA









