# Préparer un projet

Le projet est le TP ultime.

Les élèves disposent d'un cahier des charges qui fixe les attendus du projet mais ne précise pas le moyen d'y parvenir. C'est à eux de trouver leurs solutions.

Il s'agira, par exemple, de donner l'interface d'un module qu'ils devront réaliser, sans indiquer les fonctions et méthodes privées nécessaires ou les structures de données à utiliser. 



En fonction du niveau des élèves, du temps imparti ou du sujet, on pourra toutefois donner des indications pour orienter le travail.



>
>
>On choisit un sujet de projet et en avant !
>
> 





__________

Par Mieszczak Christophe

licence CC BY SA