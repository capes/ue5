# Les évaluations



## Objectif(s) d'une évaluation

Il existe deux types d'évaluations : `formative` ou `sommative`.

### Formative ?

Les évaluations `formatives` ont pour fonction de favoriser la progression des apprentissages et de renseigner l'élève et l'enseignant sur les acquis et les éléments à améliorer.

Pendant l'évaluation, l'élève doit pouvoir se rendre compte de ce qu'il a compris, de ce qu'il n'a pas compris et peut avoir la possibilité, durant l'évaluation, de faire évoluer sa compréhension et ses connaissances.

Une telle évaluation est moins stressante pour l'élève :

*  S'il de _sait_ pas, ou ne _comprends_ pas, il peut essayer d'y remédier et de progresser pendant l'évaluation qui est conçue pour cela.

* Il peut avoir accès à certains documents,  ou à l'aide d'un tiers (binôme, enseignant, AE ...).

* Le temps imparti est souvent moins stricte, le travail peut se faire en partie à la maison.

    

Revers de la médaille :

* il ne faut qu'il pense que travailler uniquement pendant l'évaluation est suffisant et il doit savoir qu'à un moment donné, il sera évalué sur ses compétences _réelles_, seul devant une copie ou un écran.
* Les notes obtenues par un élève sérieux à ce type d'évaluation sont souvent flatteuses en regard de celles qu'il pourra avoir seul avec lui même et sa copie. Il faut qu'il en soit conscient pour éviter une déconvenue et un découragement éventuels.



### Sommative ?



L'évaluation `sommative` a pour fonction l'attestation des apprentissages. Elle arrive au terme d'un processus d'enseignement (devoir final ou ... Bac) et va sanctionner ou certifier un degré de maîtrise.

Il faut qu'elle soit juste, équitable et qu'elle reflète ce qui a été travaillé auparavant. L'élève doit avoir connaissance de la date à laquelle se déroulera l'évaluation et ce sur quoi elle portera afin de pouvoir la préparer.

Il peut y avoir différents niveaux d'évaluation, de la petite interrogation au gros DS.

Affronter ces évaluation sert également d'entraînement pour :

* la  gestion de la préparation à l'épreuve, des _révisions_.
* la gestion du stress avant et pendant l'épreuve.
* la  gestion de l'effort de concentration pendant l'évaluation.



L'évaluation sommative finale est le [Baccalauréat](https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm). En NSI, il y a plusieurs épreuves :

* En première, les élèves qui décident de quitter la spécialité NSI passeront une épreuve écrite sur un QCM dont les questions balaient l'intégralité du programme de première. 
* En terminale, deux épreuves se dérouleront courant du mois de mars puis une en juin, le grand oral :
    * **Une épreuve écrite sur papier de 3h30 sur 12 points** : La partie écrite consiste en la résolution de trois exercices  permettant d'évaluer les connaissances et les capacités attendues  conformément aux programmes de première et de terminale de la  spécialité. Le sujet  propose cinq exercices, parmi lesquels le candidat choisit les trois  qu'il traitera. Ces cinq exercices permettent d'aborder les différentes  rubriques du programme, sans obligation d'exhaustivité. Le sujet  comprend obligatoirement au moins un exercice relatif à chacune des  trois rubriques suivantes : traitement de données en tables et bases de  données ; architectures matérielles, systèmes d'exploitation et  réseaux ; algorithmique, langages et programmation.
    * **Une épreuve pratique de 1h, sur ordinateur, pour 8 points** qui proposera deux exercices :
        * Le premier exercice consiste à programmer un algorithme figurant  explicitement au programme, ne présentant pas de difficulté  particulière, dont on fournit une spécification. Il s'agit donc de  restituer un algorithme rencontré et travaillé à plusieurs reprises en  cours de formation. Le sujet peut proposer un jeu de test avec les  réponses attendues pour permettre au candidat de vérifier son travail.
        * Pour le second exercice, un programme est fourni au candidat. Cet exercice  ne demande pas l'écriture complète d'un programme, mais permet de  valider des compétences de programmation suivant des modalités variées : le candidat doit, par exemple, compléter un programme « à trous » afin  de répondre à une spécification donnée, ou encore compléter un programme pour le documenter, ou encore compléter un programme en ajoutant des  assertions, etc.
    * Enfin, lors du **grand oral**, l'élève devra traiter deux questions en rapport avec ses deux spécialités. Les questions peuvent être liées ou distinctes.



### Préparer une évaluation : avant ou après ?



Les évaluations formatives peuvent avoir lieu tout au long de la formation, y compris au début afin de vérifier que les pré-requis sont maîtrisés et, éventuellement, remédier aux problèmes ou lacunes.



L'évaluation sommative intervient pour valider l'apprentissage et se fait donc au terme de cet apprentissage. 





## Différents supports

### Types d'évaluations



Voici une listes, non exhaustive, d'évaluations réalisables en NSI :

* Le TP/projet :
    * soit à faire seul en classe en un temps limité. 
    * soit à finir à la maison, éventuellement en binôme.
* L'interrogation de cours sur papier :
    *  en temps limité, seul et en classe.
    * en temps limité, en binôme et en classe.
* L'interrogation de codage sur papier.
* Le QCM.
    * avec documents à la maison
    * seul en classe.
* L'exposé devant la classe sur un point préparé à la maison.



> 
>
> * Quelles sont, parmi ces évaluations, celles qui sont formatives et celles qui sont sommatives ?
>
> * A quel type d'épreuve du bac préparent-elles ?
>
>     
>
> 



### Evaluer un TP





* N'y a-t-il que des TP de codage ?
* Le copier-coller étant une pratique répandue et appréciée des élèves, comment faire pour évaluer un TP en essayant de limiter ce phénomène ?





### Sur papier

* Que peut-on évaluer sur papier ?
* Quels sont les avantages et inconvénients de l'évaluation sur papier ?



### QCM



Le QCM est l'évaluation choisie pour le Bac en première. Il faut donc en faire. Vous disposez de divers outils pour automatiser ces QCM, le principal étant Pronote.



Démonstration ....









## La notation







_________

par Mieszczak Christophe	

licence CC BY SA