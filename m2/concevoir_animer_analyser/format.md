# Choix d'un format



## Le Markdown



Les documents que vous aller créer nécessitent de choisir un format.

* Il faut en choisir un qui soit fiable et pérenne. Si en plus il est libre et gratuit, c'est beaucoup mieux.

* Il doit être léger pour pouvoir être partagé sans difficulté, surtout sur un ENT qui n'aime pas trop les fichiers volumineux.

* Créer un document dans ce format doit être simple, rapide et donner un résultat propre, claire et agréable.

* Les élèves doivent pouvoir se l'approprier et le modifier (le compléter pour un TP d'introduction par exemple) facilement.

* Il va falloir, dans nos documents, incorporer des blocs de code qui devront facilement être copié et collés.

    



Le _Markdown_ est idéal :

* Il répond à tous les critères.

    

* Pour créer des cours, spécialement en informatiques, il offre de nombreux _bonus_ :

    * Permet l'utilisation de _notebook_ comme [Jupyter](https://jupyter.org/) ou de plateformes comme _Gitlab_.
    * Directement intégrable à un site web :

        * soit en installant un module Markdown.
        * soit en ajoutant quelques lignes de PHP en haut de la page.
        * soit en exportant votre document au format _HTML_ en un clic.
    * Export possible vers de multiples formats :_pdf_, _odt_ ... Au prix d'une augmentation de taille assez spectaculaire.

    

* Il permet d'enseigner aux élèves un langage à balises légères avant de passer au HTML.







## Le Markdown



### Quel éditeur ?

* On peut créer un document en Markdown avec un bloc note... Mais ce n'est pas idéal car il n'y a pas de possibilité de visualiser le rendu du code.

    

* Il existe de nombreux éditeur de texte capable d'interpréter le Markdown, sous tout système d'exploitation.

    * [Typora](https://typora.io/) est un éditeur Markdown pour Windows, Linux et Mac. La conception de ce logiciel est simple et ne propose pas de zone d’aperçu. Au lieu de  cela, **les formatages Markdown sont directement affichés dans le texte**. Afin de simplifier le travail avec Markdown, Typora offre une fonction  de saisie semi-automatique, que l’on retrouve habituellement dans les  éditeurs de code : si vous ouvrez un crochet, l’éditeur Markdown génère  automatiquement un crochet fermant. Son seul _défaut_ est d'être un logiciel propriétaire gratuit en bêta version qui deviendra peut-être payant ...

    * [Laverna](https://laverna.cc/) est un éditeur Markdown pour Linux, Mac et Windows. Ce projet open  source peut également être utilisé en ligne dans votre navigateur. Les  personnes possédant leur propre serveur trouveront même sur le site  Internet de l’application une version leur permettant d’héberger  personnellement **Laverna**. Quelle que soit la version  choisie, cet éditeur Markdown est toujours disponible gratuitement et  sans inscription. Laverna est construit de la même façon que la plupart  des autres éditeurs Markdown : le texte est saisi dans la zone de gauche et un aperçu en temps réel est affiché dans l’espace de droite (il est  toutefois possible de désactiver l’aperçu). Des boutons permettent de  faciliter le formatage de la note sur laquelle on travaille.

    *  [Remarkable](https://remarkableapp.github.io/linux.html) est un éditeur sous Linux qui fonctionne sur le même modèle que Laverna.

        

    

* Il existe des sites permettant de créer et de visualiser un document en Markdown. [Dillinger](https://dillinger.io/), libre, gratuit et sans création de compte est l'un d'eux. On peut aussi utiliser, par exemple, [StackEdit](https://stackedit.io/app#). Dans les deux cas, les liens locaux ne fonctionneront pas...



### Les titres

````markdown
			# Ceci est un titre H1
## Ceci est un titre H2
### Ceci est un titre H3
````



Testez en dessous !



### Gras, italic, indice et exposant

- `écrire entre deux apostrophes situées sous la touche 7`  permet de surligner ainsi.

- `_italique_` s'affiche ainsi : *italique*

- `**gras**` s'affiche ainsi : **gras**

- `**_gras-italique_**` s'affiche ainsi : **_gras-italique_**

  ​    




### Les listes



````markdown
* ceci est une puce niveau 1
	* ceci est une puce niveau 2 indentée à droite via la touche tabulation
		* ceci est une puce niveau 3 indentée à droite via la touche tabulation
````



Testez en dessous !





````markdown
1. ceci est une puce numérotée niveau 1 
	1. ceci est une puce numérotée niveau 2 indentée à droite via la touche tabulation
		
````



Testez en dessous !



### Les liens hypertext



````markdown
[description du lien](url du lien)
par exemple :
[le markdowns - wikipédia](https://fr.wikipedia.org/wiki/Markdown)
````



_Remarque :_ L'url peut être local ou non. 



Testez ici





### Les images



````markdown
![description de l'image](url du lien)
par exemple :
![logo markdown - source wikipédia - CC](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/208px-Markdown-mark.svg.png)
````





_Remarque :_ L'url peut être local ou non. 





Testez ici !



### Les tableaux 



````markdown
| Titre 1       |     Titre 2     |        Titre 3 |
| :------------ | :-------------: | -------------: |
| Colonne       |     Colonne     |        Colonne |
| Alignée à     |   Alignée au    |      Alignée à |
| Gauche        |     Centre      |         Droite |
````







Testez ici !



### Bloc codes



Vous ouvrez un bloc code, en commençant par une série de quatre "apostrophes du 7"  obtenu en pressant _ALT GR_ et _7_.

Voici par exemple un bloc code python ...

```txt
​```python
def vive_le_md():
	return 'vive le md !'
​```
```



... qui aura le rendu ci-dessous :

````Python
def vive_le_md():
	return 'vive le md !'
````



**Comme tout est en mode texte, le  copier-coller vers un éditeur de code marche à merveille !**







### HTML au secours



Vous voulez aller plus loin ? Le _Markdown_ est un dérivé du HTML. Si vous codez en HTML des objets plus complexes, ils seront interprétés...



le code ci-dessous ...

```html
<table>
	<tr>
        <td colspan = 2 align = center style="background-color: yellow; color : red"> un tableau plus complexe </td>
    </tr>
    <tr>
        <td style='color : blue' align = 'left'> pas mal </td>
        <td style='color : blue' align = 'right'> non ? </td>
    </tr>
</table>
```

... donnera :

<table>
	<tr>
        <td colspan = 2 align = center style="background-color: yellow; color : red"> un tableau plus complexe </td>
    </tr>
    <tr>
        <td style='color : blue' align = 'left'> pas mal </td>
        <td style='color : blue' align = 'right'> non ?</td>
    </tr>
</table>




### Latex



Pour écrire proprement des formules mathématiques on utilise le _Latex_. Encore un langage à balise, plus _fouillé_ celui là.

* On l'insère entre deux dollars `$ \frac 1 3 $`  donne $`\frac 1 3`$
* On ajoute de anti-côtes  de chaque côté de la formule pour la montrer en ligne sur Gitlab. `$`\frac 1 3 `$` donne $`\frac 1 3`$



Syntaxe usuelle :

``` latex
a \times b 
\frac {num} {dénom}
\sqrt {radicande}
a ^ {exposant}
a_{indice}

```



Pour en savoir, plus, rendez vous sur [ce site](https://www.tuteurs.ens.fr/logiciels/latex/manuel.html).



_Remarque :_

Oui, on pourrait créer un document entièrement en _Latex_... Mais ce serait bien plus fastidieux que de le faire en _Markdown_ et bien moins accessible pour les élèves.





### Exporter vos documents



Via le menu _fichier/exporter_ vous pouvez exportez vos documents en un clic vers de multiples formats ( sous linux, c'est la spécialité de Pandoc). 

Cela au prix d'une considérable augmentation de la quantité de données ...





____________

Mieszczak Christophe 

Licence CC - BY - SA





