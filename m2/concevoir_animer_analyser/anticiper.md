## Anticiper. Gestion de l'hétérogénéité et des difficultés



Lorsque vous préparer un chapitre, vous devez repérer quels sont les points qui risques de poser problème à vos élèves ou à certains d'entre eux.

Inversement, certains élèves seront plus rapides et il ne faut pas qu'ils s'ennuient !



Il faut donc anticiper, lorsque vous préparer vos séance, ces moments clés pour que chacun, selon le cas, puisse :

* avoir le temps de comprendre pour ne pas décrocher.
* allez plus loin pour ne pas s'ennuyer ... et finir par décrocher



> Proposez des _techniques_ pour gérer l'hétérogénéité :
>
> * lors d'un cours magistral
> * d'une classe inversée
> * d'un TP ou d'un projet





____________

Par Mieszczak Christophe

Licence CC BY SA