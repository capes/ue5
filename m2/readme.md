# Master 2

### Introduction

* [Les objectifs de l'année](introduction.md)

    

### Présentation du Lycée

* [le personnel d'un lycée et ses missions](lycee/organigramme.md)
* [les instances du lycée](lycee/instances.md)
* [le règlement intérieur](lycee/reglement_interieur.md)
* [le projet d'établissement](lycee/projet_etablissement.md)



### Enseigner : concevoir, animer et analyser

* [outils et ressources numériques](concevoir_animer_analyser/outils_numeriques.md)

* Concevoir un contenu

    * [choix d'un format](concevoir_animer_analyser/format.md)
    * [organiser une progression](concevoir_animer_analyser/preparer_une_progression.md)
    * [préparer un chapitre](concevoir_animer_analyser/preparer_un_chapitre.md) :
        * [animer une séance](concevoir_animer_analyser/animer_une_seance.md)
        * [spécificités de l'enseignement de l'informatique](concevoir_animer_analyser/specificites.md)
        * [anticiper : gestion de l'hérérogénéité, gestion des difficultés](concevoir_animer_analyser/anticiper.md)
        * [le cours magistral](concevoir_animer_analyser/cours.md)
        * [La classe inversée](concevoir_animer_analyser/cours_classe_inversee.md)
        * [préparer un TP](concevoir_animer_analyser/tp.md)
        * [préparer un projet](concevoir_animer_analyser/projet.md)
        * [évaluer](concevoir_animer_analyser/evaluations.md)
    * [et les compétences ?](concevoir_animer_analyser/competences.md)
    



___________

# Fil



|    date    | horaire |                           contenu                            |
| :--------: | :-----: | :----------------------------------------------------------: |
| 16/09/2020 |   2h    |      Contact - survole du lycée (normalement vu en M1)       |
| 23/09/2020 |   2h    | outils numériques, choix d'un format, organiser une progression |
| 07/10/2020 |   2h    |                  organiser une progression                   |
| 14/10/2020 |   4h    | propositions de progressions - animer une séance - spécificité de l'informatique |
| 04/11/2020 |   2h    |                     préparer un chapitre                     |
| 05/11/2020 |   4h    |         Visite en situation de `Mickaël Lechantre`.          |
| 02/12/2020 |   2h    |   Préparer un chapitre : présentation du travail en cours.   |
| 16/12/2020 |   2h    |     suite préparation d'un chapitre - comment évaluer ?      |
| 13/01/2020 |   2h    | Préparation d'un chapitre suite. Documents à renvoyer sous quinzaine. |
| 31/03/2020 |   2h    | CR sur les documents renvoyés. Discussion autour des sujets du CAPES |









_________

Par Mieszczak Christophe

Licence CC BY SA  

  

