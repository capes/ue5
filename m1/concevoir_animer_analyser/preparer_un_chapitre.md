# Préparer un chapitre





Vous allez ici préparer des chapitres de votre progression de A à Z en l'adaptant en fonction de spécificités de vos élèves, de votre établissement et de la répartition classe entière / demi-groupe.

* Quels points du BO  allez-vous traiter ? (précisez-le).
* Quels sont les prérequis ?
* De quels outils informatiques disposez-vous (vidéo / machines / logiciels)
* Le cours : 
    * comment l'introduire (TP / classe inversée / cours magistral) ?
    * comment le consolider ?
    * Quelle trace en restera-t-il ?
* un TP ou un projet (voir deux) selon les besoins et niveau des élèves. A préparer en classe ou/et à la maison.
* Plusieurs types d'évaluations (QCM / Papier / TP ou projet) à placer à différents moments.



Une fois la séance préparée, vous la présenterez devant le groupe.



Une discussion s'établira où vous justifierez vos choix et répondrez aux questions diverses du public.







__________

Par Mieszczak Christophe

Licence CC BY SA















