# Spécificités de l'informatique





## Salle informatique ou classe non équipée ?



L'idéal est, évidemment, de travailler en petit groupe dans une classe équipée d'ordinateur. Mais ce ne sera pas forcément le cas.

Dans ce cas, on privilégiera les TP en salle informatique et le cours en classe entière.... sauf que faire sans arrêt des cours va rapidement ennuyer les élèves : il va falloir faire des exercices bien entendu mais aussi de de l'informatique _débranchée_.



### Exercices



Certains chapitres se prêtent bien aux séances d'exercices classiques, sur papier.



En voici un exemple :



* [en première, autour de la représentation des entiers ](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/representations/representation_des_entiers/exos.md)
* [en première, autour des algorithmes kkn](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/algorithmique/k_plus_proches_voisins/exos_deconnectes.md)



### Informatique débranchées



Ce sont des chercheurs néo-zélandais qui les premiers ont mis en place il y a quelques années un programme  d’enseignement des fondements de l’informatique sans ordinateur.
 Il s’agit de transmettre quelques notions de base de façon ludique, et  sans aucun recours à l’ordinateur : on utilise des cartes, des balles,  du papier...
 L’objectif est de saisir le sens même de la pensée informatique avec les concepts fondamentaux qui la constitue :

-  qu’est-ce qu’un algorithme ?
-  qu’est-ce qui fait qu’un algorithme est meilleur qu’un autre ?
-  comment coder et transmettre une information.

Le document libre de droit : « [L’informatique sans ordinateur](https://interstices.info/upload/csunplugged/CSUnplugged_fr.pdf) » décrit avec précision la philosophie de cette démarche et propose toute une série d’activités pour les élèves à partir de l’école primaire. Allez y jeter un oeil.



Voici d'autres liens intéressants :

-  Retrouvez [les activités au format vidéo](https://primabord.eduscol.education.fr/construire-la-pensee-algorithmique) réalisées par [PIXEES](https://pixees.fr/)
-  La [vidéo](https://www.youtube.com/watch?v=4gpmdAmdJgA&feature=youtu.be) produite par Canopé pour expliquer ce qu’est un algorithme.
-  Une [animation](https://files.inria.fr/mecsci/grains-videos3.0/videos/18-algorithmes.mp4) produite par PIXEES pour expliquer aux enfants ce qu’est un algorithme.
-  Sur Eduscol, [des scénarios pédagogiques](http://cache.media.eduscol.education.fr/file/Machine_a_trier/72/3/RA16_C3_SCTE_2_machine_trier_V2_572723.pdf) pour mettre en œuvre des activités d’informatique débranchée.



Exemples de TP débranchés :

* [en seconde, avec l'algorithme de Djkstra](https://framagit.org/tofmzk/informatique_git/-/blob/master/snt/localisation/dijkstra.md)

* [en seconde, autour des données](https://framagit.org/tofmzk/informatique_git/-/blob/master/snt/donnees_structurees/anonymat/groupes_sanguins.md)

* [en première, autour de l'assembleur](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/architecture/microprocesseurs/M_10.md)

    

    



## Logiciels et modules Python



Il faut du matériel Physique mais également sous forme logiciel. Installer de nouveaux logiciels en salle informatique pose souvent beaucoup de problème. Dans l'idéal, il faut anticiper.



> 
>
> Quels modules indispensables faut-il pour mener à bien l'année en seconde, première et terminale ?
>
> Faites en une liste !



Le BO précise que les élèves doivent utiliser (au moins à un moment de l'année) un OS libre... mais la plupart des machines de l'éducation nationale sont sous _Windows_ : comment palier ce problème ?



> 
>
> Proposez des solutions _réalisables_ au lycée dans les salles informatiques.
>
> 



Quels autres logiciels vous sont indispensables ? 



> 
>
> Dresser une liste de logiciels indispensables pour la SNT ou la NSI.
>
> Surtout ceux qui ne possèdent pas de version _portables_.
>
> 







## Les machines



Une partie du programme nécessite l'utilisation de _machines_. Lesquelles ? ce ne peut être imposé de manière nationale ou même académique en raison de l'obligation légale, dans ce cas de figure, de faire un appel d'offres.



Les lycées gèrent donc cela en autonomie : Carte micro:bit, rasberry , arduino.... Puisque le Python est au coeur du programme, il vaut mieux favoriser celles que l'on programme dans ce langage.



Il est plus que probable que vous utilisiez les mêmes machines en seconde. Il faudra donc bien penser, dans vos progressions à la gestion de ce matériel pour que tous les niveaux ne fassent pas la même chose au même moment.





## Vous n'êtes pas seuls



Vous n'êtes, _probablement_, pas les seuls à faire de l'informatique dans votre établissement. Peut être y-a-t-il une spécialité _SI_ ou une filière _SIN_ en Sti2d. 



Dans les deux cas, les enseignants y ont du matériel et du savoir faire. De quoi mener des projets interdisciplinaire ou partager du matériel.



Le grand oral du bac, à partir de 2021, demandera aux élèves de répondre à deux questions, jugées fondamentales,  dans chacune de leurs spécialités. Encore une fois, un travail inter-disciplinaire va s'imposer. Bien souvent, ce sera NSI-Math ou NSI-SI mais il y a des couples improbables.







__________

Par Mieszczak Christophe

licence CC BY SA