# Préparer un cours magistral

Il s'agit ici de préparer un cours magistral.

Il doit être clair, faire participer les élèves en prenant en compte l'hétérogénéité (exemples de difficultés bien dosées, prise de parole des élèves), varier les supports (avec une vidéo par exemple) et ne pas durer trop longtemps. 

La posture magistrale n'est pas la préférée des élèves. Il faut donc l'utiliser lorsque cela est nécessaire pour des notions particulières. Si votre présence n'est pas indispensable, une classe inversée ou un TP fera mieux l'affaire.



Voici un [exemple de cours magistral](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/architecture/microprocesseurs/architecture_van_neuman.md) . 



> * Notez les parties où l'élèves est mis en activité
> * Quelle(s) partie(s) pourrai(en)t être donnée(s) en classe inversée ?
> * Quelles _astuces_ permettent de varier la présentation ?
> * Comment est gérée l'hétérogénéité ?



### Classe inversée



Il s'agit ici de proposer un cours ou un TP/cours à faire travailler aux élèves chez eux en autonomie. La difficulté doit être bien dosée, le sujet bien choisi (certain passages très techniques, comme faire l'inventaire de méthodes d'une liste par exemple, sont idéaux pour cela).

On peut également réaliser de courtes vidéos, des _capsules_ permettant de résumé nue partie de cours.



















__________

Par Mieszczak Christophe

licence CC BY SA