# Outils et ressources numériques



## ENT



Les lycées de l'académie utilisent tous le même l' **E** _space_ **N** _umérique de_ **T** _ravail_ : [celui là](https://connexion.enthdf.fr/?callBack=https%3A%2F%2Fenthdf.fr%2F). 

Il s'agit d'un contrat, renouvelable, ou pas, ente l'académie et la société qui fournit l'ENT. 



Lorsque vous êtes fonctionnaire stagiaire ,c'est à dire titulaire d'un CAPES, vous obtenez une adresse mail académique du type _prenom.nom@ac-lille.fr_. Ensuite, une fois nommé dans un établissement, on vous fournit des identifiants permettant de vous connecter à cet ENT.



Grâce à lui vous pouvez :

* Communiquer avec les élèves et personnels **de l'établissement** via une messagerie dédiée **interne**.
* Utiliser diverses applications :
    * Pronote ... pour les notes entre autres choses (il propose également une messagerie, un cahier de texte et divers outils intéressants)
    * un _casier_ et un espace documentaire pour partager des documents.
    * un forum .
    * ...



**Il faut savoir :**

*  qu'il est interdit d'imposer l'utilisation d'une application quelconque à nos élèves mineurs si celle-ci demande une création de compte.
* qu'il est interdit de collecter les adresses mails (ni tout autre donnée personnelle d'ailleurs).



En conséquence, l'utilisation de l'ENT est quasi obligatoire pour échanger mails et données entre élèves et enseignants. 

Vous trouverez [ICI](ent.md) un petit tuto à son sujet.





## Eduline



Il ne s'agit pas réellement d'un outils mais d'un portail incontournable regroupant :

* votre messagerie professionnelle académique pour joindre des personnels en dehors de votre établissement : on n'utilise pas de mail personnel pour communiquer.
* les liens vers les outils indispensables au suivi de vos mission et de votre  carrière :
    * suivi des examens et concours
    * Gestion des personnels 
    * formation et ressource

![eduline](media/eduline_accueil.jpg)



Vous pouvez y accéder en passant par votre ENT ou directement, avec les mêmes identifiants que pour l'ENT.



## Site de l'académie de Lille

Chaque académie à un site qui lui est propre. Celui de Lille est [ICI](http://www1.ac-lille.fr/).





Ce site ne se destine pas qu'aux enseignants mais à tous les personnels, aux parents et aux élèves.



![site académique](media/academie_lille.jpg)



Vous y trouverez :

* les informations sur le fonctionnement de l'académie et du rectorat.
* les informations sur les différents établissements de l'académie, de la maternelle au lycée
* les programmes scolaires de la maternelle au lycée
* les informations liées à l'orientation des élèves
* tout ce qui concerne les examens et concours.



## Eduction.gouv



Le site [education.gouv.fr](https://www.education.gouv.fr/programmes-scolaires-41483) est le portail de l'éducation nationale.



![education.gouv.fr](media/education_gouv.jpg)



Comme pour le site académique, il  ne se destine pas qu'aux enseignants mais à tous les personnels, aux parents et aux élèves.

Vous y trouverez tous les informations, consignes et textes officiels.

Le portail académique vous redirigera souvent vers lui.





## Eduscol



[Eduscol](https://eduscol.education.fr/index.php?./D0011/LLPDPR01.htm) est un portail proposant des informations et des contenus pour les enseignants et les élèves.

![Eduscol](media/eduscol.jpg)



Vous Y trouverez :

* Les **B**ulletins **O**fficiels.
* Des informations sur les différents parcours des élèves et la scolarité.
* des textes et informations autour de la vie des établissements
* Des informations autour de la formation continue des enseignants.
* Des contenus pédagogiques nombreux et intéressants.







## Enseignement à distance



Risque de confinement oblige, il faut se préparer à un éventuel retour du télétravail.

La loi posant certaines contraintes, en terme de protection de données notamment, il est impératif de choisir ds outils respectant le RGPD et ne nécessitant, entre autre, aucune création de compte pour les élèves.



Un établissement à intérêt à harmoniser pour l'ensemble des enseignants les outils qui seront utilisés pour le distanciel avec les élèves afin d'éviter qu'un élève, et ses parents, ne doivent gérer une multitude de techniques différentes.



Les principaux outils institutionnels sont :

* L'ENT : 
    * pour communiquer via la messagerie.
    * pour échanger des données de tailles raisonnables.
* Pronote (si l'établissement l'utilise, ce qui est le cas d'une majorité d'entre eux) au cas où l'ENT aurait des soucis :
    * pour communiquer.
    * pour ramasser des travaux
* [La classe virtuelle du CNED](https://www.cned.fr/maclassealamaison) permet de gérer une classe en vidéo-conférence :
    * le professeur s'inscrit sur le site
    * il crée une classe et génère un lien de partage
    * il envoie le lien aux élèves par mail via l'ENT ainsi que l'horaire de la séance.
    * l'élève clique sur le lien à l'heure souhaitée et rejoint la classe virtuelle.



Il existe bien entendu des alternatives :

* [Jitsi.meet](https://meet.jit.si/) est un système de vidéoconférence sécurisé, riche, gratuit et opensource.
* S'il est interdit de demander aux élèves de créer un compte, il reste possible de vous ouvrir un espace de stockage sur un cloud et de leur envoyer un lien de consultation d'un dossier ensuite. [woelkli](https://ppp.woelkli.com/login) est une bonne solution.
* L'éducation nationale a signé un contrat avec _Microsoft_. Sisi, un gros ... On vous proposera peut-être d'utiliser _microsoft Team_...



**Dans tous les cas, il faudra penser à présenter la solution retenue aux élèves avant d'en avoir impérativement besoin.**





## Partage et entre-aide



La quantité de travail à fournir est énorme.

D'où l'importance de se constituer un réseau de collègues et de partager votre travail.

Il existe des communautés sur _Facebook_ par exemple (le coin des profs de NSI, le coin des profs de SNT). Il faudra également construire la votre.



David Roch propose des contenus très intéressants :

* [SNT en seconde](https://pixees.fr/informatiquelycee/n_site/snt.html)
* [NSI en première](https://pixees.fr/informatiquelycee/n_site/nsi_prem.html)
* [NSI en terminale](https://pixees.fr/informatiquelycee/n_site/nsi_term.html)









__________

Par Mieszczak Christophe

Licence CC BY SA















