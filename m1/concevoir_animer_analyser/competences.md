# Compétences en informatique



Dans la plupart des disciplines, en particuliers en Mathématiques, il existe des grilles de compétences spécifiques à la discipline.

Ce n'est pas encore le cas en NSI et SNT, pour l'instant.



En revanche, il existe un outils pour valider des compétences générales en informatiques : [PIX](https://pix.fr/).



 Allez y jeter un coup d'oeil !



__________

Par Mieszczak Christophe

licence CC BY SA