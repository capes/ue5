# Préparer un TP

L'informatique se prête particulièrement bien aux TP.

* Il peut d'agir d'une façon de faire cours souvent plus efficace et attrayante qu'un cours magistral. L'élève découvre alors les notions au fur et à mesure qu'il en a besoin et les appliquent dans sur des cas concrets, parce qu'il en a besoin.
* Il peut s'agir d'un apprentissage aux projets où les élèves réalisent un _projet plus ou moins guidé_ afin d'approfondir leur maîtrise d'une notion en travaillant en équipe, si possible.



## Exemples 



Voici [un exemple de TP/cours](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/representations/representation_des_caracteres/codages_des_caracteres.md)  autour du codage des caractères qui permet de découvrir le cours, avec un peu de classe inversée (où ça ??) en travaillant en HTML et CSS.



> * Quelles notions sont abordées ici.
>
>     * thème principal : codage des caractères
>
>     * base du HTML / CSS 
>
>     * notions retravaillées : codage binaire (UTF-8) et rappel sur les licences (sources images)
>
>         
>
> * Pourquoi choisir d'utiliser le HTML pour présenter ce cours ?
>
>     * déjà vu en SNT (normalement ...), indispensable en 1NSI dans certaines parties. Permet aussi de voir un langage de codage qui n'est pas un langage de programmation.
>
> * Comment est gérée l'hétérogénéité ?
>
>     * chacun avance à son rythme dans le TP. Le prof fait un point régulier.
>     * le prof peut passer voir le travail des élèves individuellement et aider ceux qui ont le plus de difficultés. 
>     * les liens permettent aux élèves qui le souhaitent/peuvent d'aller un peu plus loin
>     * en HTML et CSS le travail demandé est assez basique. Mais des élèves qui auraient plus de connaissances/motivation peuvent très bien en faire plus.
>
> * Quelle(s) partie(s) pourraient se faire en classe inversée ?
>
>     * on peut demander de lire chez eux les parties de cours sur ASCII, ISO et UTF8. Il faudra y revenir en classe, mais beaucoup plus vite.
>
>         
>
> * Imaginiez des exercices débranchés pour compléter ce TP ?
>
>     * parler des polices de caractères.
>     * exercices sur le décodage d'une série d'octets dans différents type de codages.
>     * en python : fonctions `chr` et `ord`



Voici un [autre  TP/cours](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/types_construits/listes.md) sur les listes.



> * Quelles sont les points importants abordés ?
>     * qu'est-ce qu'une liste / tableau indexé
>     * parcourir une liste
>     * le `in`
>     * choix d'un élément au hasard : important en Maths.
>     * générer une liste
>     * méthodes
>     * mutabilités
>     * copier une liste
>     * paradigme fonctionnel est entrevu (éviter les effets de bord)
> * Quelle(s) partie(s) pourraient se faire en classe inversée ?
>     * tester des commandes dans la console
>     * regarder en détails des méthodes des listes
>     * le travail doit être simple et pas trop long à faire (ils ont des TP aussi ...)
> * Comment est gérée l'hétérogénéité ?
>     * chacun avance à son rythme dans le TP. Le prof fait un point régulier.
>     * le prof peut passer voir le travail des élèves individuellement et aider ceux qui ont le plus de difficultés. 
>     * les liens permettent aux élèves qui le souhaitent/peuvent d'aller un peu plus loin
>     * les exercices de codage vont _ralentir_ les élèves les plus rapide et permettre au groupe de _recoller_.
> * Donnez des exemples de TP d'approfondissement qui pourraient se faire après ce cours.
>     * [cf la liste des projets](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/readme.md)
> *  Tout faire d'un coup ? Non.
>     * On peut couper le cours avant la partie mutabilité et commencer, par exemple les dictionnaire ou un TP/projet, avant de revenir aux listes.



Voici [un TP](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/projets/nim/nim.md) , autour du jeu de Nim , qui n'apporte pas de nouvelle notion mais servira à solidifier et approfondir les notions apprises en cours.



> * Quelles notions interviennent ici ?
>     * les bases de programmation :
>         * variables locales
>         * fonctions et découpage en petits problèmes/fonctions
>         * conditions
>         * boucles
> * Comment pourrait-on gérer l'hétérogénéité ?
>     * Travail de groupe : deux élèves se répartissent les tâches et s'entre_aident si nécessaire.



Voici [un TP](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/projets/les_amis/les_amis.md) sur l'utilisation des structures liste et dictionnaire.

>  
>
>  * Quand donner ce TP ?
>      * après avoir vu les méthodes des listes et avant la mutabilité
>      * revenir après avoir vu les dictionnaires.
>
>  * Quel est l'intérêt de ce TP ?
>  * montrer que le choix d'une bonne structure de données est primordiale
>    * montrer que le découpage en petites fonctions permet de ne modifier que quelques fonctions lors du changement de structure.
>    * on évoque les graphes, vu en SNT (vocabulaires : sommets, arêtes ...)



Voici un dernier [TP débranché](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi/architecture/microprocesseurs/M_999.md) sur le M-999.



> 
>
> Quels sont les intérêts de faire du débranché ?
>
> 



## A vous





Réalisez un TP/cours sur un des sujets suivants au choix :

* Les dictionnaires
* les chaînes de caractères
* les tris



Réalisez un TP d'approfondissement sur un le sujet choisi.



__________

Par Mieszczak Christophe

licence CC BY SA