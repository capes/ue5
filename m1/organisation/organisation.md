# Organisation de l'année



En master 1, vous suivrez deux stages accompagnés de deux semaines aux attendus différents.

* Le premier stage est un stage de découverte et d'observation
* Lors du second, vous serez confrontés à l'acte d'enseigner.



**Le stage 1 ne délivre pas de crédit mais une évaluation peut être demandée. En revanche, après le stage 2, l’UE 5 et l’UE 4 sont évaluées conjointement via un dossier**, de 15 pages maximum,  comportant nécessairement les parties suivantes :

* Présentation du contexte d’établissement
* Analyse d’une séquence d’enseignement menée (analyse de l’activité de l’élève et de sa propre pratique)
* Mise en relation des phénomènes vécus et observés avec les cours de l’UE 4.



## Premier stage



### AVANT le stage

* Vous devrez prendre rendez-vous avec votre tuteur pour :
    * en premier lieu faire connaissance.
    * visiter l'établissement.
    * Récupérer les documents indispensables à l'analyse demandée :
        * projet d'établissement
        * projet d'équipe et action mise en place, en SNT ou en NSI.
        * portrait des classes que vous allez suivre.
        * Partie de la progression que vous allez suivre dans les différentes classes.
    * Décider avec le tuteur d'une séquence à préparer dans la classe que le tuteur jugera la plus adaptée ( Le tuteur pourra accepter ou non de vous la laisser présenter aux élèves).
    * Planifier les rendez-vous avec les personnels éducatifs qui auront lieu pendant le stage.



### Préparation du stage à l'université



Afin de vous préparer à ces stages, nous travaillerons autour de plusieurs thèmes :

* Le fonctionnement d'un établissement :

    * Organigramme hiérarchique et mission des personnels

    * Les instances (CA, CP ...)

    * Le projet d'établissement. 

    * Les projets disciplinaires.

    * Outils numériques (ent, eduline,eduscol ...)

        

* Préparer une séquence :

    * le BO

    * Les compétences

    * Adaptation aux particularités de la classe et de l'établissement

    * Pré-requis et objectifs (en terme de programme et de compétences). Insertion dans une progression.

    * Construction de la séquence en prenant en considération les différents publics concernés :

        * cours
        * TP
        * Mini-projet
        * évaluation

    * Présentation de séquences préparées par des étudiants. Discussion autour des séquences.

      
    
* Questions diverses des étudiants :

  * 
  
    ​    

### Pendant le stage 



* Entretien avec les personnels suivant rendez-vous préparés avec le tuteur.
* Analyse avec le tuteur terrain du projet d'établissement.
* Analyse des outils numériques utilisés
* Observation et analyse des leçons 
* Discussion avec le tuteur de la séance préparée. Eventuellement, sur décision du tuteur, présentation de la séance aux élèves.
* Si possible, participation à un conseil de classe.



### Après le stage



* Partage d'expérience.
* Mise en évidence de stratégies pédagogiques différentes selon le public.
* Analyse du projet d'établissement et création d'un projet qui s'y intègre.
* Rédaction d'un premier dossier qui pourra servir de base au dossier final.



## Stage 2



### AVANT le stage

* Décision, avec le tuteur, de séances que le stagiaire préparera et présentera aux élèves. A prévoir environ 4h d'intervention devant élève par semaine :

    * cours

    * mise en activités :  exercices, mini-projet ou TP

    * évaluation

        

* Discussion avec le tuteur sur les attente de ce dernier quant à ces interventions.

* classes dans lesquelles intervenir (seconde/première/terminale)



### Préparation à l'université.



* préparation des séquences décidées par le tuteur.

    * insertion dans la progression (BO - prés-requis)

    *  adaptation à un public large : gérer l'hétérogénéité.

    *  adaptation aux spécificités de l'établissement (matériel, classe entière / demi-groupe, outils numériques ...)

        

* Gestion de classe :

    * Règlement intérieur

    * Gestion de l'hétérogénéité

    * Gestion d'un conflit

      
      
        

* Présentation des séances devant les étudiants et retour des étudiants après les présentations.



### Pendant le stage

* Poursuite du travail d'observation

* Prise en main des séances d'enseignement prévues avec le tuteur.

* Avec le tuteur terrain, bilan critique des séances.

* Si possible, observation de la classe dans une autre discipline pour découvrir d'autres dispositifs pédagogiques.

* Si possible, participation à un conseil de classe

    

    

### Après le stage

* Partage d'expérience
* mise au point du dossier.



_________

Par Mieszczak Christophe

Licence CC BY SA



