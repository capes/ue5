# Mahara



Il ne s'agit pas de faire un descriptif détaillé de l'application mais de pouvoir s'en servir rapidement.

Pour entrer dans les détails, [suivez ce lien](https://manual.mahara.org/fr/20.04/)





## Lancer l'application



Connectez vous sur l'ENT de l'inspé.

Dans la partie `Mes enseignements`, choisissez le Moodle et pas mahara :

![mahara](media/mahara.jpg)



Connectez vous,en cliquant sur _Connexion_ en haut à droite, puis, dans la colonne de gauche,  tout en bas, sélectionnez `Accueil _ Mahara`.



![accueil de Mahara](media/accueil_mahara.jpg)



Vous voilà dans l'accueil de votre portfolio. Vous pouvez choisir d'`Ecrire` une nouvelle _page_ ou en modifier une existante, de `Déposer` des fichiers, d'`Organiser` vos pages en _collections_ et de `Partager` vos pages ou collections.

![le menu d'accueil](media/accueil.jpg)

## Créer / modifier un article

Cliquez sur la pièce de _puzzle_ `Ecrire `. Vous arrivez alors dans l'accueil du journal. 

Le *journal* est un outil qui permet de capturer, vos réflexions  et vos expériences. En ajoutant, plus tard, l’ensemble d’un journal, ou des articles de celui-ci, à une page, vous pouvez rendre ces informations  accessibles à d’autres utilisateurs. Ces derniers peuvent alors  commenter et participer à vos travail réflexif.



Vous pouvez

* `Ajouter un article`  en cliquant sur le bouton du même nom.
* Modifier un article existant :
    * soit en le faisant passé de _Brouillon_ (donc invisible aux visiteurs) à _publié_ (ou inversement).
    * soit en modifier le contenu en cliquant sur l'icône _style_ .
    * soit en le supprimant en cliquant sur l'icône _poubelle_.

![créer une page](media/accueil_journal.jpg)



Ajoutons un nouvel article. 

* On indique son _Titre_
* On remplit le _Corps_ de l'article 
* On peut y indiquer (ou non), des mots clés pour retrouver cet article parmi les nombreux que vous écrirez.
* On peut y joindre un ou plusieurs fichiers, en cliquant sur le bouton adéquate
* On peut décider de passer l'article en mode _Brouillon_ (invisible) ou _Publié_ (visible).
* On peut autoriser (ou non) les commentaires : **à faire pour faciliter le suivi de votre stage !**
* Enfin, pour terminer, il reste à  `Enregistrer l'article` .

![Titre et description](media/nouvel_article.jpg)



Vous revenez alors à la page précédente d'où vous pouvez, soit ajouter une autre page, soit modifier vos articles comme indiqué plus haut.



## Déposer des Documents

L'intérêt de déposer des documents à cet endroit plutôt que (ou en plus de les mettre) dans un article est de pouvoir les retrouver plus facilement, sans avoir à rechercher l'article dans lequel il est insérer.



Pour cela, on clique sur la pièce de puzzle `Déposer` et ... il n'y a plus qu'à aller chercher le fichier concerner, de façon très classique.

Le fichier est importé dans le dossier courant qui est, par défaut, _Accueil_. Mais vous pouvez créer une arborescence de dossiers, à partir de cette racine, pour organiser vos données, en cliquant sur le bouton `Créer un dossier` .  Il est également possible de naviguer dans l'arborescence dans la partie basse de la page.

![importer un fichier](media/importer_fichier.jpg)







### A suivre ............







________

Par Mieszczak Christophe

CC BY SA