# Mahara



Il ne s'agit pas de faire un descriptif détaillé de l'application mais de pouvoir s'en servir rapidement.

Pour entrer dans les détails, [suivez ce lien](https://manual.mahara.org/fr/20.04/)





## Lancer l'application



Connectez vous sur l'ENT de l'inspé.

Dans la partie `Mes enseignements`, choisissez le Moodle et pas mahara :

![mahara](media/mahara.jpg)



Connectez vous,en cliquant sur _Connexion_ en haut à droite, puis, dans la colonne de gauche,  tout en bas, sélectionnez `Accueil _ Mahara`.



![accueil de Mahara](media/accueil_mahara.jpg)



Vous voilà dans l'accueil de votre portfolio. Vous pouvez choisir d'`Ecrire` une nouvelle _page_ ou en modifier une existante, de `Déposer` des fichiers, d'`Organiser` vos pages en _collections_ et de `Partager` vos pages ou collections.

![le menu d'accueil](media/accueil.jpg)

## Créer / modifier un article

Cliquez sur la pièce de _puzzle_ `Ecrire `. Vous arrivez alors dans l'accueil du journal. 

Le *journal* est un outil qui permet de capturer, vos réflexions  et vos expériences. En ajoutant, plus tard, l’ensemble d’un journal, ou des articles de celui-ci, à une page, vous pouvez rendre ces informations  accessibles à d’autres utilisateurs. Ces derniers peuvent alors  commenter et participer à vos travail réflexif.



Vous pouvez

* `Ajouter un article`  en cliquant sur le bouton du même nom.
* Modifier un article existant :
    * soit en le faisant passé de _Brouillon_ (donc invisible aux visiteurs) à _publié_ (ou inversement).
    * soit en modifier le contenu en cliquant sur l'icône _style_ .
    * soit en le supprimant en cliquant sur l'icône _poubelle_.

![créer une page](media/accueil_journal.jpg)



Ajoutons un nouvel article. 

* On indique son _Titre_
* On remplit le _Corps_ de l'article 
* On peut y indiquer (ou non), des mots clés pour retrouver cet article parmi les nombreux que vous écrirez.
* On peut y joindre un ou plusieurs fichiers, en cliquant sur le bouton adéquate
* On peut décider de passer l'article en mode _Brouillon_ (invisible) ou _Publié_ (visible).
* On peut autoriser (ou non) les commentaires : **à faire pour faciliter le suivi de votre stage !**
* Enfin, pour terminer, il reste à  `Enregistrer l'article` .

![Titre et description](media/nouvel_article.jpg)



Vous revenez alors à la page précédente d'où vous pouvez, soit ajouter une autre page, soit modifier vos articles comme indiqué plus haut.



## Déposer des Documents

L'intérêt de déposer des documents à cet endroit plutôt que (ou en plus de les mettre) dans un article est de pouvoir les retrouver plus facilement, sans avoir à rechercher l'article dans lequel il est insérer.



Pour cela, on clique sur la pièce de puzzle `Déposer` et ... il n'y a plus qu'à aller chercher le fichier concerner, de façon très classique.

Le fichier est importé dans le dossier courant qui est, par défaut, _Accueil_. Mais vous pouvez créer une arborescence de dossiers, à partir de cette racine, pour organiser vos données, en cliquant sur le bouton `Créer un dossier` .  Il est également possible de naviguer dans l'arborescence dans la partie basse de la page.

![importer un fichier](media/importer_fichier.jpg)







### A suivre ............



## Organiser des collections

Vous allez peut être créer un article par séance et ils vont vite être très nombreux. Il va falloir s'organiser !

Revenez à l'accueil et cliquez sur la pièce de puzzle `Organiser`.

![s'organiser](media/organiser.jpg)



On va alors regrouper des articles dans une _collection_. Cela vous permettra de vous y retrouver plus facilement mais aussi de faciliter le partage ds vos articles.

Cliquez donc sur le bouton `Ajouter` puis sur `Collection` :

* Nommez la collection
* ajoutez-y une courte description
* cliquez ensuite sur `Suivant..` pour y ajouter vos articles.



![modifier_collection](media/creer_collection.jpg)













## Partager votre travail





Vous pouvez décider de la mise en page de votre article en sélectionnant un modèle particulier.

![mise en page](media/presentation.jpg)



Il n'y a _plus qu'à_ modifier le contenu. 

Vous pouvez ajouter, en utilisant le menu de la colonne de gauche, du texte, des images, des fichiers divers (trace de cours, vidéo ...). Prenez un maximum de notes concernant votre stage. Elles vous serviront de base à la création de votre dossier. N'hésitez pas à y inclure des photos de cahier d'élèves (anonymement), des remarques ou questions d'élèves et toutes les notes que vous pouvez avoir concernant le cours , l'attitude des élèves ou celle de l'enseignant....



Une fois terminé, cliqué sur le bouton ... `Terminer`. Vous pourrez toujours y revenir plus tard, comme cela est expliqué plus haut, en cliquant sur l'icône _stylo_ sur la liste de pages à l'accueil du portfolio.![modifier le contenu](media/modifier_contenu.jpg)



Il faut maintenant partager vos notes avec moi et votre tuteur terrain. 

Comme vous allez avoir un grand nombre de pages (une par jour), nous allons créer une collection. Pour cela, suivez le menu _Portfolio/Collection_ puis cliquez sur `Nouvelle collection`. Remarquez, comme pour les pages, que votre collection,une fois crée, sera modifiable en cliquant sur l'icône _stylo_ à l'accueil du portfolio.

![creer_collection](media/collection.jpg)



Commençons par donner un titre et une description à notre collection. Cliquez ensuite sur le bouton `Suivant : Ajouter des pages à la collection`.

![nommer la collection](media/titre_collection.jpg)



Nous allons compléter notre collection en y ajoutant les pages au fur et à mesure (Lorsque vous aurez de nouvelles pages, il faudra revenir ici, en cliquant sur le bouton _stylo_ dont nous avons parlé plus haut). Sélectionnez les pages à ajouter en cochant les cases correspondantes puis cliquez sur le bouton `Terminer`.

![Ajouter des pages](media/ajouter_pages.jpg)

Voilà. La collection est prête. Il ne reste qu'à créer un lien de partage. Pour cela, suivez le menu _Portfolio/Partagée **par** moi_ . Cliquez ensuite sur l'icône en forme de cadenas, en bout de ligne, sous _URLs secrètes_.

![Partager](media/partager_collection.jpg)



Créez l'_URL secrète_ en cliquant sur le bouton `Ajouter`. Il n'y a plus qu'à la copier et à l'envoyer par mail à votre tuteur terrain et moi.

![URL secrète](media/URL_secrete.jpg)



Allez, au TAF !!



________

Par Mieszczak Christophe

CC BY SA