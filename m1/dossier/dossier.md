# Evaluation UE5, semestre 2

## Cadrage



Vous trouverez [Le cadrage officiel](eval_ue5_s2.pdf) en suivant ce lien.



Voici quelques exemple de dossiers :

* [Celui de Philippe BODDAERT ](Rapport de stage.pdf)



## Oui mais sinon ...

### Validations des semestres



* Le semestre 1 est validé, en UE4 et UE5, par le seul dossier de l'UE4.

* Le semestre 2 est validé, en UE4 et UE5, par le dossier de l'UE4 et la présence obligatoire aux 2 stages.

    

### Modalité du dossier UE4 



A partir des expériences en stage, se donner un objectif pédagogique et l'analyser à travers les situations d’apprentissages mises en œuvre.

* **Ce n'est pas un rapport de stage**, mais une réflexion au sujet d'une activité pédagogique observée ou un cours mené. 
* **contrainte** : prendre appui sur contenu de l'UE4 (didactique, détaché de la matière)

* On vous demande un travail _réflexif_. La _réflexivité_ est l'aptitude à analyser sa propre activité. Cela signifie que vous devez mener une réflexion sur votre propre pratique du métier et de l'informatique, l'analyser de façon à comprendre comment la faire évoluer, l'améliorer... On pourra, par exemple, se baser sur des travaux d'élèves ou leur attitude en classe pour changer telle ou telle façon de faire, tel ou tel posture en classe.

* les dossiers seront déposés sur Moodle au plus tard le 16 avril.

    

### Différences entre les dossiers UE4 et UE5 :



**UE4** : 

Techniques didactiques non liées à la discipline et donc transposables dans différents enseignements.



**UE5** : 

* Les éléments disciplinaires, la façon d'enseigner l'informatique avec ses spécificités, la mise en place des contenus sont au coeur du dossier.
* On demande un travail _réflexif_. En plus de préparer le contenu, il faut réfléchir à ses choix, à son activité  :
    * Pendant la construction du contenu :
        * près-requis ? Inscription dans la progression ? Importance dans la suite du programme ?
        * pourquoi le présenter de cette façon ? y-a-t-il d'autres moyens ?
        * comment gérer les particularités du public (hétérogénéité, autonomie, différenciation ...)
        * comment gérer les particularités de l'établissement (taille de groupe, matériel ...)
        * ...
    * Pendant le cours :
        * gardez en mémoire :
            * les réactions des élèves : comprennent-ils ? participent-ils ? 
            * des idées qui vous viennent suite à une question d'élève par exemple.
            * des problèmes auxquels vous n'aviez pas pensé.
            * ...
    * Après le cours :
        * travaux d'élèves ..
        * réussites et échecs de la séances.
        * remédiation.
        * ...





## Mahara



L'utilisation de [Mahara](http://ent.inspe-lille-hdf.fr/f/welcome/normal/render.uP) imposée pour le stage :

* Le portfolio n'est pas pris en compte dans l'évaluation, c'est un outils. Mais il deviendra obligatoire en 2021, d'où la décision de l'utiliser dès cet année.
* Il s'agit d'y écrire quotidiennement le déroulement de votre stage afin de pouvoir vous appuyer dessus pour la rédaction du dossier :
    * ce que vous y faite
    * les séances que vous voyez/préparez
    * les rencontres avec les personnels
    * des travaux d'élèves.
    * des discussions, remarques avec des élèves ou des enseignants
    * vos idées en vrac .....
* Il faudra donner une _URL secrète_ à votre tuteur terrain et à moi, afin que nous puissions suivre vos travaux.



Vous trouverez [ICI](mahara.md) un petit tuto sur son utilisation.

__________

Par Mieszczak Christophe

Licence CC BY SA

