# Master 1

* [Organisation de l'année](organisation/organisation.md)

    

* Présentation du Lycée

    * [le personnel d'un lycée et ses missions](lycee/organigramme.md)
    * [les instances du lycée](lycee/instances.md)
    * [le règlement intérieur](lycee/reglement_interieur.md)
    * [le projet d'établissement](lycee/projet_etablissement.md)

    

* Observer, analyser :

    * [qu'observer ?](observer/observer.md)
    * [débriefer une séquence](observer/debriefer.md)
    
    
    
* Concevoir, animer :

    * [choix d'un format](concevoir_animer_analyser/format.md)

    * [outils et ressources numériques](concevoir_animer_analyser/outils_numeriques.md)

    * [Les BO et les progressions](concevoir_animer_analyser/preparer_une_progression.md)

    * [animer une séance](concevoir_animer_analyser/animer_une_seance.md)

    * concevoir un contenu :

        * [preparer un TP](concevoir_animer_analyser/tp.md)
        
        * [preparer un cours magistral](concevoir_animer_analyser/cours.md)
        
            
        
          ​    
        
    
* Dossier UE5

  * [A propos du dossier](dossier/dossier.md)
  * [Tuto Mahara](dossier/mahara.md)
  
  
  
    
  
  ​    

_________

## Fil



|    date    | horaire |                           contenu                            |
| :--------: | :-----: | :----------------------------------------------------------: |
| 16/09/2020 |   2h    | Contact - organigramme du lycée : les personnels et leurs missions |
| 23/09/2020 |   2h    |                    Les instances du lycée                    |
| 07/10/2020 |   2h    |         règlement intérieur, projet d'établissement,         |
| 21/10/2020 |   4h    | BO, progression, animer une séance - Rappel : préparer l'observation et le débriefing en stage |
| 12/11/2020 |   2h    |                        Préparer un TP                        |
| 19/11/2020 |   2h    | Mise au point dossier d'évaluation UE5 (cadrage, explications), Utilisation de Mahara |
| 10/12/2020 |   2h    | Compte rendu des stages - lien dossier ue4 et ue5 - attendus détaillés du dossier |
| 16/12/2020 |   2h    | préparation réflexive d'une partie d'un chapitre en s'appuyant sur l'expérience du stage 1 |
| 13/01/2021 |   2h    |                    Préparation du stage.                     |
| 11/02/2021 |   2h    |                 CR stage. Plans de dossiers.                 |
| 31/03/2021 |   2h    | CR sujet de Capes, dernière mise au point sur les dossiers.  |
|            |         |                                                              |





________

Par Mieszczak Christophe

Licence CC BY SA