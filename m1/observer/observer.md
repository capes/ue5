# Qu'observer ?





Vous allez _observer_ plusieurs séances menées par un enseignant aguerri, en SNT ou en NSI. Mais que faut-t-il observer ?



## Conditions de travail



Il est ici question de noter dans quelles conditions les élèves et l'enseignant travaillent :

* Combien d'élèves en SNT ? En première NSI ? En terminale NSI ?

* Le cours est-il toujours en classe entière ? toujours en demi-groupe ? 

* De quel matériel disposent-ils ? (salles informatiques / Micro:bit ? / Arduino / ? Rasberry ? ...... )

* Quel est le système d'exploitation utilisé en salle informatique ? Quels sont les logiciels utilisés ?

* De quoi l'enseignant aurait bien besoin alors qu'il n'en dispose pas ? 

    



## Déroulement du cours



* Comment l'enseignant accueille-t-il les élèves ?

    ```txt
    
    ```

    

* De quelle tâches administratives doit-il s'acquitter ?

* Comment commence-t-il la séance ? 

* Quelle(s) posture(s) utilise-t-il lors du déroulement de la séance ?



## Côté élèves

 Comment se comportent les élèves : Sont-ils calmes ? Participent-ils ? Sont-ils actifs ? Posent-ils des questions ? Sont-ils autonomes ? 

* Lors de l'entrée en classe ?
* Lors des phases de cours magistral ? 
* Lors des TP ?



Y-a-t-il beaucoup de travail à la maison ?

* Sous quel forme ?
* fréquence ?



Quel est le niveau réel des élèves en regard des attentes du programme ?

* le contenu leur parait-il abordable ? difficile ?
* Y-a-t-il des difficultés liées à la COVID ? Lesquelles ?



## Le contenu



* Quelles sont les notions travaillées ?

* Quelles étaient les prés-requis ? Plus précisément, dans quelle progression s'inscrivent ces contenus ?

* Quelles difficultés rencontrent les élèves ?

* Comment l'enseignant gère-t-il l'hétérogénéité ?

    



















___________

Par Mieszczak Christophe

Licence CC BY SA