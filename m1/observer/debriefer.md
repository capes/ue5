# Débriefer





C'est l'heure de l'entretien avec votre tuteur terrain. Que lui demander ? Voici quelques pistes



## Analyse terrain



* Condition de travail au lycée : suite à vos observation, demander comment il s'adapte aux conditions de travaille dans son établissement (groupe, classe entière, matériel ...)

* Y-a-t-il un travail d'équipe ou des projets concernant la NSI ? la SNT ? Un travail inter-disciplinaire ?

* Qu'en est-il de la formation continue ?

* Que faudrait-t-il pour améliorer ces conditions de travail (matériel ? formation ? dédoublement ? ....) 

    

## Analyse du public



* Quel est le niveau moyen d'un élève en SNT ? en NSI ?
* Quel intérêt manifestent les élèves vis à vis de la matière ? En SNT ? En NSI ?
* Quel type de comportement(s) ont les élèves dans cet établissement ?  Y-a-t-il des problèmes particuliers ? 
* Comment est jugée la difficulté du programme en NSI par le professeur ? les élèves ?
* Quelle orientation envisagent les élèves qui choisissent NSI en première ? en terminale ?



## Analyse du contenu



* Pourquoi le tuteur a-t-il choisi sa progression ?
* Quels étaient les prés-requis de la séance ?
* Que se passe-t-il ensuite ?
* Quelle séquence (pas forcément un cours complet...) pourriez-vous préparer (et éventuellement présenter) ?



## Analyse des postures



* Pourquoi choisir telle ou telle posture ?
* Quelles postures fonctionnent le mieux ? Dans quels cas ?
* Y-a-t-il des choses à faire ou ne pas faire ?
* Quelle posture vous conseille-t-il d'adopter pour votre intervention  ?





___________

Par Mieszczak Christophe

Licence CC BY SA